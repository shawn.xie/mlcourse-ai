{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center>\n",
    "<img src=\"../../img/ods_stickers.jpg\" />\n",
    "    \n",
    "## [mlcourse.ai](mlcourse.ai) – Open Machine Learning Course \n",
    "Author: [Yury Kashnitskiy](https://yorko.github.io) (@yorko). Edited by Anna Tarelina (@feuerengel). This material is subject to the terms and conditions of the [Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license. Free use is permitted for any non-commercial purpose."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center>Assignment #3. Fall 2018\n",
    "## <center> Decision trees for classification and regression"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**In this assignment, we will find out how a decision tree works in a regression task, then will build and tune classification decision trees for identifying heart diseases.\n",
    "Fill in the missing code in the cells marked \"You code here\" and answer the questions in the [web form](https://docs.google.com/forms/d/1hsrNFSiRsvgB27gMbXfQWpq8yzNhLZxuh_VSzRz7XhI).**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "from matplotlib import pyplot as plt\n",
    "from sklearn.model_selection import train_test_split, GridSearchCV\n",
    "from sklearn.metrics import accuracy_score\n",
    "from sklearn.tree import DecisionTreeClassifier, export_graphviz\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. A simple example of regression using decision trees"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider the following one-dimensional regression problem. It is needed to build the function $a(x)$ to approximate original dependency $y = f(x)$ using mean-squared error $min \\sum_i {(a(x_i) - f(x_i))}^2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = np.linspace(-2, 2, 7)\n",
    "y = X ** 3\n",
    "\n",
    "plt.scatter(X, y)\n",
    "plt.xlabel(r'$x$')\n",
    "plt.ylabel(r'$y$');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make several steps to build the decision tree. Let's choose the symmetric thresholds equal to 0, 1.5 and -1.5 for partitioning. In the case of a regression task, the leaf outputs mean answer for all observations in this leaf."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start from tree of depth 0 that contains all train observations. How will predictions of this tree look like for $x \\in [-2, 2]$? Create the appropriate plot using a pen, paper and Python if it is needed (without using `sklearn`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "plt.scatter(X, y)\n",
    "plt.plot(X, np.mean(X).repeat(len(X)))\n",
    "plt.xlabel(r'$x$')\n",
    "plt.ylabel(r'$y$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's split the data according to the following condition $[x < 0]$. It gives us the tree of depth 1 with two leaves. Let's create a similar plot for predictions of this tree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "plt.scatter(X, y)\n",
    "left = X[X < 0]\n",
    "right = X[X >= 0]\n",
    "a = np.r_[np.mean(left).repeat(len(left)), np.mean(right).repeat(len(right))]\n",
    "plt.plot(X, a)\n",
    "plt.xlabel(r'$x$')\n",
    "plt.ylabel(r'$y$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the decision tree algorithm, the feature and the threshold for splitting are chosen according to some criterion. The commonly used criterion for regression is based on variance: $$\\large Q(X, y, j, t) = D(X, y) - \\dfrac{|X_l|}{|X|} D(X_l, y_l) - \\dfrac{|X_r|}{|X|} D(X_r, y_r),$$\n",
    "where $\\large X$ and $\\large y$ are a feature matrix and a target vector (correspondingly) for training instances in a current node, $\\large X_l, y_l$ and $\\large X_r, y_r$ are splits of samples $\\large X, y$ into two parts w.r.t. $\\large [x_j < t]$ (by $\\large j$-th feature and threshold $\\large t$), $\\large |X|$, $\\large |X_l|$, $\\large |X_r|$ (or, the same, $\\large |y|$, $\\large |y_l|$, $\\large |y_r|$) are sizes of appropriate samples, and $\\large D(X, y)$ is variance of answers $\\large y$ for all instances in $\\large X$:\n",
    "$$\\large D(X) = \\dfrac{1}{|X|} \\sum_{j=1}^{|X|}(y_j – \\dfrac{1}{|X|}\\sum_{i = 1}^{|X|}y_i)^2$$\n",
    "Here $\\large y_i = y(x_i)$ is the answer for the $\\large x_i$ instance. Feature index $\\large j$ and threshold $\\large t$ are chosen to maximize the value of criterion  $\\large Q(X, y, j, t)$ for each split.\n",
    "\n",
    "In our 1D case,  there's only one feature so $\\large Q$ depends only on threshold $\\large t$ and training data $\\large X$ and $\\large y$. Let's designate it $\\large Q_{1d}(X, y, t)$ meaning that the criterion no longer depends on feature index $\\large j$, i.e. in 1D case $\\large j = 0$.\n",
    "\n",
    "Create the plot of criterion $\\large Q_{1d}(X, y, t)$  as a function of threshold value $t$ on the interval $[-1.9, 1.9]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def regression_var_criterion(X, y, t):\n",
    "    # You code here\n",
    "    left_index = X < t\n",
    "    right_index = ~left_index\n",
    "    total_var = np.var(y - np.mean(y))\n",
    "    left_var = np.var(y[left_index] - np.mean(y[left_index]))\n",
    "    right_var = np.var(y[right_index] - np.mean(y[right_index]))\n",
    "    n = float(len(X))\n",
    "    left_n = sum(1 for e in left_index if e == True)\n",
    "    right_n = sum(1 for e in right_index if e == True)\n",
    "    return total_var - left_n / n * left_var - right_n / n * right_var"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "thresholds = np.linspace(-1.9, 1.9, 21)\n",
    "Q = [regression_var_criterion(X, y, t) for t in thresholds]\n",
    "plt.plot(thresholds, Q)\n",
    "plt.xlabel(r'$t$')\n",
    "plt.ylabel(r'$Q$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font color='red'>Question 1.</font> Is the threshold value $t = 0$ optimal according to the variance criterion?**\n",
    "- Yes\n",
    "- No "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then let's make splitting in each of the leaves' nodes. In the left branch (where previous split was $x < 0$) using the criterion $[x < -1.5]$, in the right branch (where previous split was $x \\geqslant 0$) with the following criterion $[x < 1.5]$. It gives us the tree of depth 2 with 7 nodes and 4 leaves. Create the plot of these tree predictions for $x \\in [-2, 2]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "import collections\n",
    "\n",
    "class DecisionTree:\n",
    "    \"\"\"决策树: 支持且仅支持手动创建。\"\"\"\n",
    "    def __init__(self, threshold, feature=0):\n",
    "        \"\"\"条件判断形式为: X[feature] < threshold \"\"\"\n",
    "        self.threshold = threshold\n",
    "        self.feature = feature\n",
    "        self.left_node = None\n",
    "        self.right_node = None\n",
    "        self._value = None\n",
    "        # 可以通过修改这个成员来控制如何计算叶子的值\n",
    "        self._value_f = np.mean\n",
    "\n",
    "    def fit(self, X, y):\n",
    "        \"\"\"根据训练数据，手动的、递归的计算叶子节点的value。\n",
    "        如果X是多维，则行表示记录，列表示特征\"\"\"\n",
    "        # leaf\n",
    "        if self.left_node == None and self.right_node == None:\n",
    "            self._value = self._value_f(y)\n",
    "            return self\n",
    "        # otherwise\n",
    "        ## make X a matrix\n",
    "        if len(X.shape) == 1:\n",
    "            X = X.reshape((X.shape[0], 1))\n",
    "        ## split\n",
    "        index = X[:, self.feature] < self.threshold\n",
    "        ## call children's fit\n",
    "        if self.left_node is not None:\n",
    "            self.left_node.fit(X[index, :], y[index])\n",
    "        if self.right_node is not None:\n",
    "            rindex = ~index\n",
    "            self.right_node.fit(X[rindex, :], y[rindex])\n",
    "        return self\n",
    "\n",
    "    def predict(self, x):\n",
    "        # 如果x是数组，遍历之\n",
    "        if isinstance(x, collections.Iterable):\n",
    "            return np.array([self.predict(e) for e in x])\n",
    "        # leaf\n",
    "        if self.left_node == None and self.right_node == None:\n",
    "            return self._value\n",
    "        # otherwise\n",
    "        is_left = x < self.threshold\n",
    "        if is_left:\n",
    "            return self.left_node.predict(x)\n",
    "        return self.right_node.predict(x)\n",
    "\n",
    "\n",
    "# Create the decision tree\n",
    "dtree = DecisionTree(0)\n",
    "dtree.left_node = DecisionTree(-1.5)\n",
    "dtree.right_node = DecisionTree(1.5)\n",
    "dtree.fit(X, y)\n",
    "\n",
    "xx = np.linspace(-2, 2, 41)\n",
    "yy = dtree.predict(xx)\n",
    "plt.plot(xx, yy)\n",
    "plt.scatter(xx, xx ** 3, c='r')\n",
    "plt.xlabel(r'$X$')\n",
    "plt.ylabel(r'$y$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font color='red'>Question 2.</font> How many segments are there on the plot of tree predictions in the interval [-2, 2] (it is necessary to count only horizontal lines)?**\n",
    "- 2\n",
    "- 3\n",
    "- 4\n",
    "- 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Building a decision tree for predicting heart diseases\n",
    "Let's read the data on heart diseases. The dataset can be downloaded from the course repo from [here](https://github.com/Yorko/mlcourse.ai/blob/master/data/mlbootcamp5_train.csv) by clicking on `Download` and then selecting `Save As` option.\n",
    "\n",
    "**Problem**\n",
    "\n",
    "Predict presence or absence of cardiovascular disease (CVD) using the patient examination results.\n",
    "\n",
    "**Data description**\n",
    "\n",
    "There are 3 types of input features:\n",
    "\n",
    "- *Objective*: factual information;\n",
    "- *Examination*: results of medical examination;\n",
    "- *Subjective*: information given by the patient.\n",
    "\n",
    "| Feature | Variable Type | Variable      | Value Type |\n",
    "|---------|--------------|---------------|------------|\n",
    "| Age | Objective Feature | age | int (days) |\n",
    "| Height | Objective Feature | height | int (cm) |\n",
    "| Weight | Objective Feature | weight | float (kg) |\n",
    "| Gender | Objective Feature | gender | categorical code |\n",
    "| Systolic blood pressure | Examination Feature | ap_hi | int |\n",
    "| Diastolic blood pressure | Examination Feature | ap_lo | int |\n",
    "| Cholesterol | Examination Feature | cholesterol | 1: normal, 2: above normal, 3: well above normal |\n",
    "| Glucose | Examination Feature | gluc | 1: normal, 2: above normal, 3: well above normal |\n",
    "| Smoking | Subjective Feature | smoke | binary |\n",
    "| Alcohol intake | Subjective Feature | alco | binary |\n",
    "| Physical activity | Subjective Feature | active | binary |\n",
    "| Presence or absence of cardiovascular disease | Target Variable | cardio | binary |\n",
    "\n",
    "All of the dataset values were collected at the moment of medical examination."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('../../data/mlbootcamp5_train.csv', \n",
    "                 index_col='id', sep=';')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Transform the features: create \"age in years\" (full age) and also create 3 binary features based on `cholesterol` and 3 more on `gluc`, where they are equal to 1, 2 or 3. This method is called dummy-encoding or One Hot Encoding (OHE). It is more convenient to use `pandas.get_dummmies.`. There is no need to use the original features `cholesterol` and `gluc` after encoding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "# an utils function\n",
    "def add_prefix_into_columns(df, prefix):\n",
    "    df.columns = [\"%s_%r\"%(prefix, col) for col in df.columns]\n",
    "    return df\n",
    "\n",
    "# age\n",
    "df.age = np.array(df.age/365, dtype=np.int64)\n",
    "# cholesterol\n",
    "chlt_df = add_prefix_into_columns(pd.get_dummies(df.cholesterol), 'cholesterol')\n",
    "del df['cholesterol']\n",
    "# glucose\n",
    "glc_df = add_prefix_into_columns(pd.get_dummies(df.gluc), 'gluc')\n",
    "del df['gluc']\n",
    "# concatenate three dataframes\n",
    "df = pd.concat([df, chlt_df, glc_df], axis=1)\n",
    "df.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.describe().T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Split data into train and holdout parts in the proportion of 7/3 using `sklearn.model_selection.train_test_split` with `random_state=17`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "# X_train, X_valid, y_train, y_valid = ...\n",
    "X_index = pd.Index(set(df.columns) - set(['cardio']))\n",
    "X = df[X_index]\n",
    "y = df['cardio']\n",
    "X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.3, random_state=17)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Train the decision tree on the dataset `(X_train, y_train)` with max depth equals to 3 and `random_state=17`. Plot this tree with `sklearn.tree.export_graphviz`, `dot` and `pydot`. You don't need to use quotes in the file names in order to make it work in a jupyter notebook. The commands starting from the exclamation mark are terminal commands that are usually run in terminal/command line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "cd_tree = DecisionTreeClassifier(max_depth=3, random_state=17)\n",
    "cd_tree.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use .dot format to visualize a tree\n",
    "from ipywidgets import Image\n",
    "from io import StringIO\n",
    "import pydotplus #pip install pydotplus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dot_data = StringIO()\n",
    "export_graphviz(cd_tree, feature_names=X.columns.values,\n",
    "                out_file=dot_data, filled=True)\n",
    "graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  \n",
    "Image(value=graph.create_png())\n",
    "# -"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font color='red'>Question 3.</font> What 3 features are used to make predictions in the created decision tree?**\n",
    "- weight, height, gluc=3\n",
    "- smoke, age, gluc=3\n",
    "- age, weight, chol=3\n",
    "- age, ap_hi, chol=3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make predictions for holdout data `(X_valid, y_valid)` with the trained decision tree. Calculate accuracy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "cd_tree_pred = cd_tree.predict(X_valid)\n",
    "before_gs_score = accuracy_score(y_valid, cd_tree_pred)\n",
    "before_gs_score"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set up the depth of the tree using cross-validation on the dataset `(X_train, y_train)` in order to increase quality of the model. Use `GridSearchCV` with 5 folds. Fix `random_state=17` and change  `max_depth` from 2 to 10."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tree_params = {'max_depth': list(range(2, 11))}\n",
    "\n",
    "cd_tree_grid = GridSearchCV(cd_tree, tree_params, cv=5, n_jobs=-1, verbose=True)\n",
    "cd_tree_grid.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Draw the plot to show how mean accuracy is changing in regards to `max_depth` value on cross-validation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "plt.plot(np.arange(2, 11), cd_tree_grid.cv_results_['mean_test_score'])\n",
    "plt.grid()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Print the best value of `max_depth` where the mean value of cross-validation quality metric reaches maximum. Also compute accuracy on holdout data. All these computations are possible to make using the trained instance of the class `GridSearchCV`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "cd_tree_grid.best_params_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd_tree_grid.best_score_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "after_gs_score = accuracy_score(cd_tree_grid.predict(X_valid), y_valid)\n",
    "after_gs_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(after_gs_score - before_gs_score) / before_gs_score * 100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**<font color='red'>Question 4.</font> Is there a local maximum of accuracy on the built validation curve? Did `GridSearchCV` help to tune `max_depth` so that there's been at least 1% change in holdout accuracy?**\n",
    "(check out the expression (acc2 - acc1) / acc1 * 100%, where acc1 and acc2 are accuracies on holdout data before and after tuning `max_depth` with `GridSearchCV` respectively)?\n",
    "- yes, yes\n",
    "- yes, no \n",
    "- no, yes\n",
    "- no, no"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a look at the SCORE table to estimate ten-year risk of fatal cardiovascular disease in Europe. [Source paper](https://academic.oup.com/eurheartj/article/24/11/987/427645).\n",
    "\n",
    "<img src='../../img/SCORE2007-eng.png' width=70%>\n",
    "\n",
    "Create binary features according to this picture:\n",
    "- $age \\in [40,50), \\ldots age \\in [60,65) $ (4 features)\n",
    "- systolic blood pressure: $ap\\_hi \\in [120,140), ap\\_hi \\in [140,160), ap\\_hi \\in [160,180),$ (3 features)\n",
    "\n",
    "If the values of age or blood pressure don't fall into any of the intervals then all binary features will be equal to zero. Then we create decision tree with these features and additional ``smoke``, ``cholesterol``  and ``gender`` features. Transform the ``cholesterol`` to 3 binary features according to it's 3 unique values ( ``cholesterol``=1,  ``cholesterol``=2 and  ``cholesterol``=3). This method is called dummy-encoding or One Hot Encoding (OHE). Transform the ``gender`` from 1 and 2 into 0 and 1. It is better to rename it to ``male`` (0 – woman, 1 – man). In general, this is typically done with ``sklearn.preprocessing.LabelEncoder`` but here in case of only 2 unique values it's not necessary.\n",
    "\n",
    "Finally the decision tree is built using 12 binary features (without original features).\n",
    "\n",
    "Create a decision tree with the limitation `max_depth=3` and train it on the whole train data. Use the `DecisionTreeClassifier` class with fixed `random_state=17`, but all other arguments (except for `max_depth` and `random_state`) should be set by default.\n",
    "\n",
    "**<font color='red'>Question 5.</font> What binary feature is the most important for heart disease detection (it is placed in the root of the tree)?**\n",
    "- Systolic blood pressure from 160 to 180 (mmHg)\n",
    "- Gender male / female\n",
    "- Systolic blood pressure from 140 to 160 (mmHg)\n",
    "- Age from 50 to 55 (years)\n",
    "- Smokes / doesn't smoke\n",
    "- Age from 60 to 65 (years)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You code here\n",
    "# update gender\n",
    "df.gender = df.gender - 1\n",
    "df.rename(columns={'gender': 'male'}, inplace=True)\n",
    "\n",
    "def change_age_to_label(df):\n",
    "    \"\"\"update the age of dataframe with 4 features\"\"\"\n",
    "    age_1 = np.zeros(len(df), dtype=np.uint8)\n",
    "    age_2 = np.zeros(len(df), dtype=np.uint8)\n",
    "    age_3 = np.zeros(len(df), dtype=np.uint8)\n",
    "    age_4 = np.zeros(len(df), dtype=np.uint8)\n",
    "    age = df.age\n",
    "    one_index = (age >= 40) & (age < 50)\n",
    "    two_index = (age >= 50) & (age < 55)\n",
    "    three_index = (age >= 55) & (age < 60)\n",
    "    four_index = (age >= 60) & (age < 65)\n",
    "    age_1[one_index] = 1\n",
    "    age_2[two_index] = 1\n",
    "    age_3[three_index] = 1\n",
    "    age_4[four_index] = 1\n",
    "    del df['age']\n",
    "    df.insert(0, 'age_1', age_1)\n",
    "    df.insert(1, 'age_2', age_2)\n",
    "    df.insert(2, 'age_3', age_3)\n",
    "    df.insert(3, 'age_4', age_4)\n",
    "    return df\n",
    "\n",
    "def change_aphi_to_label(df):\n",
    "    \"\"\"update the ap_hi of dataframe with 3 features\"\"\"\n",
    "    aphi_1 = np.zeros(len(df), dtype=np.uint8)\n",
    "    aphi_2 = np.zeros(len(df), dtype=np.uint8)\n",
    "    aphi_3 = np.zeros(len(df), dtype=np.uint8)\n",
    "    aphi = df.ap_hi\n",
    "    one_index = (aphi >= 120) & (aphi < 140)\n",
    "    two_index = (aphi >= 140) & (aphi < 160)\n",
    "    three_index = (aphi >= 160) & (aphi < 180)\n",
    "    aphi_1[one_index] = 1\n",
    "    aphi_2[two_index] = 1\n",
    "    aphi_3[three_index] = 1\n",
    "    del df['ap_hi']\n",
    "    df.insert(0, 'ap_hi_1', aphi_1)\n",
    "    df.insert(1, 'ap_hi_2', aphi_2)\n",
    "    df.insert(2, 'ap_hi_3', aphi_3)\n",
    "    return df\n",
    "\n",
    "def get_labeled_age(age):\n",
    "    \"\"\"Get a dataframe with labeled age\"\"\"\n",
    "    new_age = age.copy()\n",
    "    zero_index = (age < 40) | (age >= 65)\n",
    "    one_index = (age >= 40) & (age < 50)\n",
    "    two_index = (age >= 50) & (age < 55)\n",
    "    three_index = (age >= 55) & (age < 60)\n",
    "    four_index = (age >= 60) & (age < 65)\n",
    "    new_age[zero_index] = 0\n",
    "    new_age[one_index] = 1\n",
    "    new_age[two_index] = 2\n",
    "    new_age[three_index] = 3\n",
    "    new_age[four_index] = 4\n",
    "    new_age = pd.get_dummies(new_age, drop_first=True, prefix='age')\n",
    "    return new_age\n",
    "\n",
    "def get_labeled_aphi(aphi):\n",
    "    \"\"\"Get a dataframe with labeled age\"\"\"\n",
    "    new_aphi = aphi.copy()\n",
    "    zero_index = (aphi < 120) | (aphi >= 180)\n",
    "    one_index = (aphi >= 120) & (aphi < 140)\n",
    "    two_index = (aphi >= 140) & (aphi < 160)\n",
    "    three_index = (aphi >= 160) & (aphi < 180)\n",
    "    new_aphi[zero_index] = 0\n",
    "    new_aphi[one_index] = 1\n",
    "    new_aphi[two_index] = 2\n",
    "    new_aphi[three_index] = 3\n",
    "    new_aphi = pd.get_dummies(new_aphi, drop_first=True, prefix='ap_hi')\n",
    "    return new_aphi\n",
    "\n",
    "selected_columns = ['age', 'male', 'ap_hi', 'smoke', 'cholesterol_1', 'cholesterol_2', 'cholesterol_3']\n",
    "df = df[selected_columns]\n",
    "# df = change_age_to_label(df)\n",
    "# df = change_aphi_to_label(df)\n",
    "age_df = get_labeled_age(df.age)\n",
    "aphi_df = get_labeled_aphi(df.ap_hi)\n",
    "del df['age']\n",
    "del df['ap_hi']\n",
    "df = pd.concat([age_df, df, aphi_df], axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cvd_tree = DecisionTreeClassifier(max_depth=3, random_state=17)\n",
    "cvd_tree.fit(df, y)\n",
    "accuracy_score(cvd_tree.predict(df), y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dot_data = StringIO()\n",
    "export_graphviz(cvd_tree, feature_names=df.columns.values,\n",
    "                out_file=dot_data, filled=True)\n",
    "graph = pydotplus.graph_from_dot_data(dot_data.getvalue())  \n",
    "Image(value=graph.create_png())"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "text_representation": {
    "extension": ".py",
    "format_name": "light",
    "format_version": "1.3",
    "jupytext_version": "0.8.3"
   }
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  },
  "name": "lesson4_part2_Decision_trees.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
