# -*- coding: utf-8 -*-
# ---
# jupyter:
#   anaconda-cloud: {}
#   jupytext_format_version: '1.3'
#   jupytext_formats: py:light
#   kernelspec:
#     display_name: Python [conda env:anaconda2]
#     language: python
#     name: conda-env-anaconda2-py
#   language_info:
#     codemirror_mode:
#       name: ipython
#       version: 2
#     file_extension: .py
#     mimetype: text/x-python
#     name: python
#     nbconvert_exporter: python
#     pygments_lexer: ipython2
#     version: 2.7.13
# ---

# <center>
# <img src="../../img/ods_stickers.jpg">
# ## Open Machine Learning Course
# <center>Author: [Yury Kashnitsky](https://www.linkedin.com/in/festline/), Data Scientist @ Mail.Ru Group <br>
# Translated and edited by [Sergey Isaev](https://www.linkedin.com/in/isvforall/), [Artem Trunov](https://www.linkedin.com/in/datamove/), [Anastasia Manokhina](https://www.linkedin.com/in/anastasiamanokhina/), and [Yuanyuan Pao](https://www.linkedin.com/in/yuanyuanpao/) <br>All content is distributed under the [Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

# # <center> Assignment #1 (demo)
# ## <center>  Exploratory data analysis with Pandas
#

# **In this task you should use Pandas to answer a few questions about the [Adult](https://archive.ics.uci.edu/ml/datasets/Adult) dataset. (You don't have to download the data – it's already  in the repository). Choose the answers in the [web-form](https://docs.google.com/forms/d/1uY7MpI2trKx6FLWZte0uVh3ULV4Cm_tDud0VDFGCOKg).**

# Unique values of all features (for more information, please see the links above):
# - `age`: continuous.
# - `workclass`: Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked.
# - `fnlwgt`: continuous.
# - `education`: Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc, 9th, 7th-8th, 12th, Masters, 1st-4th, 10th, Doctorate, 5th-6th, Preschool.
# - `education-num`: continuous.
# - `marital-status`: Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent, Married-AF-spouse.
# - `occupation`: Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces.
# - `relationship`: Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried.
# - `race`: White, Asian-Pac-Islander, Amer-Indian-Eskimo, Other, Black.
# - `sex`: Female, Male.
# - `capital-gain`: continuous.
# - `capital-loss`: continuous.
# - `hours-per-week`: continuous.
# - `native-country`: United-States, Cambodia, England, Puerto-Rico, Canada, Germany, Outlying-US(Guam-USVI-etc), India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican-Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El-Salvador, Trinadad&Tobago, Peru, Hong, Holand-Netherlands.   
# - `salary`: >50K,<=50K

import numpy as np
import pandas as pd
pd.set_option('display.max.columns', 100)
# to draw pictures in jupyter notebook
# %matplotlib inline 
import matplotlib.pyplot as plt
import seaborn as sns
# we don't like warnings
# you can comment the following 2 lines if you'd like to
import warnings
warnings.filterwarnings('ignore')

data = pd.read_csv('../../data/adult.data.csv')
data.head()

# **1. How many men and women (*sex* feature) are represented in this dataset?** 

# You code here
print "Answer 1: There are %d mem and %d women"%(len(data[data.sex == 'Male']), len(data[data.sex == 'Female']))

# **2. What is the average age (*age* feature) of women?**

# You code here
print "Answer 2: %.2f"%data[data.sex == 'Female'].age.mean()

# **3. What is the percentage of German citizens (*native-country* feature)?**

# You code here
print "Answer 3: %.2f"%((100.0 * len(data[data['native-country'] == 'Germany'])) / len(data))

# **4-5. What are the mean and standard deviation of age for those who earn more than 50K per year (*salary* feature) and those who earn less than 50K per year? **

# You code here
a1 = data[data.salary == '>50K'].age
a2 = data[data.salary == '<=50K'].age
print "Answer 4-5: >50k: %.0f, %.1f;"%(a1.mean(), a1.std()),
print "<=50k: %.0f, %.1f"%(a2.mean(), a2.std())

# **6. Is it true that people who earn more than 50K have at least high school education? (*education – Bachelors, Prof-school, Assoc-acdm, Assoc-voc, Masters* or *Doctorate* feature)**

# You code here
# set(data[data.salary == '>50K'].education)
print "Answer 6: No"

# **7. Display age statistics for each race (*race* feature) and each gender (*sex* feature). Use *groupby()* and *describe()*. Find the maximum age of men of *Amer-Indian-Eskimo* race.**

# You code here
print data.groupby('race')['age'].describe()
print data.groupby('sex')['age'].describe()

# **8. Among whom is the proportion of those who earn a lot (>50K) greater: married or single men (*marital-status* feature)? Consider as married those who have a *marital-status* starting with *Married* (Married-civ-spouse, Married-spouse-absent or Married-AF-spouse), the rest are considered bachelors.**

# You code here
# 下面两种方法都可以达到同样的结果，个人觉得还是apply好，这样可以少记一些API
# married = data[data['marital-status'].str.startswith('Married')]
married = data[data['marital-status'].apply(lambda x: x.startswith('Married'))]
# singled = data[~data['marital-status'].str.startswith('Married')]
singled = data[data['marital-status'].apply(lambda x: not x.startswith('Married'))]
print "Answer 8 is ",
print 'Married: %.1f%%; Singled: %.1f%%'%(100.0 * len(married[married.salary == '>50K']) / len(married), \
                                          100.0 * len(singled[singled.salary == '>50K']) / len(singled))

# **9. What is the maximum number of hours a person works per week (*hours-per-week* feature)? How many people work such a number of hours, and what is the percentage of those who earn a lot (>50K) among them?**

# You code here
print "Answer 9 is ",
print data['hours-per-week'].max(),
a1 = data[data['hours-per-week'] == 99]
print len(a1),
a2 = a1[a1.salary == '>50K']
print "%.0f%%"%(100.0 * len(a2) / len(a1))

# **10. Count the average time of work (*hours-per-week*) for those who earn a little and a lot (*salary*) for each country (*native-country*). What will these be for Japan?**

# You code here
a1 = data.groupby(['native-country', 'salary'])['hours-per-week'].mean()
print a1
print '---- Japan ------'
print a1.loc['Japan']
