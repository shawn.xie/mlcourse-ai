<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="mlcourse.ai" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1541598714900" COLOR="#ff9900" BACKGROUND_COLOR="#cccccc"><hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="6" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Assignments" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_422500660" CREATED="1540893963058" MODIFIED="1541516328377" HGAP_QUANTITY="9.500000134110447 pt" VSHIFT_QUANTITY="-38.99999883770946 pt">
<edge COLOR="#0000ff"/>
<node TEXT="assignment 4" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1008486707" CREATED="1540893999810" MODIFIED="1541516481196" TEXT_SHORTENED="true"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Linear Regression
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x4e00;&#x4e9b;&#x7075;&#x5149;&#x4e00;&#x95ea;" ID="ID_1923063693" CREATED="1541252424852" MODIFIED="1541252443991">
<node TEXT="&#x4e3a;&#x4ec0;&#x4e48;&#x4e0d;&#x53ef;&#x4ee5;&#x5bf9;&#x672c;&#x4f5c;&#x4e1a;&#x4e2d;&#x7684;&#x6570;&#x636e;&#x8fdb;&#x884c;Pooling&#x64cd;&#x4f5c;?" ID="ID_1763006843" CREATED="1541252513655" MODIFIED="1541252602992"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21453;&#36807;&#26469;&#21487;&#20197;&#21457;&#29616;&#65292;&#19981;&#33021;&#22240;&#20026;Pooling&#23481;&#26131;&#29702;&#35299;&#32780;&#24573;&#35270;&#20102;&#23545;&#23427;&#30340;&#24605;&#32771;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="CNN&#x7684;&#x5927;&#x91cf;&#x8ba1;&#x7b97;&#x90fd;&#x662f;&#x7ebf;&#x6027;&#x7684;&#xff0c;&#x9664;&#x4e86;Relu&#x8fd9;&#x6837;&#x7b80;&#x5355;&#x7684;&#x8fd0;&#x7b97;&#xff0c;&#x90a3;&#x4e48;&#x53ef;&#x4ee5;&#x7528;&#x7c7b;&#x4f3c;&#x7684;&#x65b9;&#x6cd5;&#x5904;&#x7406;XOR&#x6570;&#x636e;&#x5417;?" ID="ID_1472824690" CREATED="1541252709060" MODIFIED="1541252746527"/>
</node>
<node TEXT="Exploratory Data Analysis" ID="ID_1724245572" CREATED="1540894009433" MODIFIED="1540970817250" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/3d60f623-3232-4108-b0f5-62b3c953db5d?title=Exploratory%20data%20analysis%20-%20Wikipedia"/>
<node TEXT="Sparse Matrix" ID="ID_9777860" CREATED="1540970600574" MODIFIED="1540970957771" LINK="https://docs.scipy.org/doc/scipy-0.18.1/reference/sparse.html"/>
<node TEXT="Feature Engine" ID="ID_409600690" CREATED="1540979474467" MODIFIED="1540979683402"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#26080;&#27861;&#30452;&#25509;&#20351;&#29992;Alice&#25968;&#25454;&#65292;&#22240;&#20026;&#20803;&#32032;&#26159;&#32593;&#31449;&#30340;ID&#65292;&#36825;&#20123;ID&#34429;&#28982;&#20855;&#26377;&#25968;&#20540;&#30340;&#24418;&#24335;&#65292;&#20294;&#24182;&#19981;&#26159;&#25968;&#12290;&#25152;&#20197;&#38656;&#35201;&#23558;&#25968;&#25454;&#20174;session X site_NO&#36716;&#20026;session X site_ID&#65292;&#20803;&#32032;&#20174;&#32593;&#31449;ID&#36716;&#21464;&#20026;&#35775;&#38382;&#27425;&#25968;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x7279;&#x5f81;&#x5f52;&#x4e00;&#x5316;" ID="ID_834063966" CREATED="1541224259204" MODIFIED="1541224309187" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/4f1856f9-6dec-4169-ba75-09cdac6b57b7/">
<node TEXT="&#x4e3a;&#x4ec0;&#x4e48;&#x9700;&#x8981;&#x5f52;&#x4e00;&#x5316;?" ID="ID_1760991289" CREATED="1541251135356" MODIFIED="1541251283382"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#26368;&#30452;&#25509;&#30340;&#30097;&#24785;&#19981;&#26159;&#21487;&#20197;&#36890;&#36807;&#26435;&#20540;&#26469;&#25511;&#21046;&#21527;? &#20026;&#20160;&#20040;&#38656;&#35201;&#20154;&#20026;&#30340;&#24402;&#19968;&#21270;&#21442;&#25968;&#21602;? &#19968;&#20010;&#31572;&#26696;&#26159;
    </p>
    <ul>
      <li>
        &#22240;&#20026;&#26377;&#35268;&#25972;&#21270;&#65292;&#36825;&#20250;&#24809;&#32602;&#36807;&#22823;&#30340;&#26435;&#20540;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x5bf9;&#x7a00;&#x758f;&#x77e9;&#x9635;&#x7684;&#x5f52;&#x4e00;&#x5316;" ID="ID_1255692097" CREATED="1541303357085" MODIFIED="1541303505212"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20351;&#29992;StandardScaler&#26080;&#27861;&#22788;&#29702;&#31232;&#30095;&#30697;&#38453;&#65292;&#21487;&#20197;&#20351;&#29992;Normalizer&#65292;&#21442;&#32771;&#36825;&#20010;&#22238;&#31572;(https://stackoverflow.com/questions/20240068/scaling-issues-with-scipy-sparse-matrix-while-using-scikit)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Linear Trend" ID="ID_387923209" CREATED="1541050963892" MODIFIED="1541251347892" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/07c65b3e-e7b8-4e81-8bb6-db0f084b30c8/">
<node TEXT="&#x5bf9;&#x65f6;&#x95f4;&#x6570;&#x636e;&#x7684;&#x5904;&#x7406;" ID="ID_1097119964" CREATED="1541334066264" MODIFIED="1541334110774"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        &#20363;&#22914;&#23558;&#26102;&#38388;&#36716;&#25442;&#20026;&quot;&#24180;&#26376;&quot;&#65292;&#25968;&#25454;&#31867;&#22411;&#25913;&#25104;float&#65292;&#24182;&#36827;&#34892;&#24402;&#19968;&#21270;(StandardScaler)
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x6570;&#x636e;&#x805a;&#x5408;" ID="ID_1929753295" CREATED="1541149462969" MODIFIED="1541149489168">
<node TEXT="&#x900f;&#x89c6;&#x8868;(Pivot Table)" ID="ID_879526836" CREATED="1541149142779" MODIFIED="1541151449022" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/713fad32-d395-43d1-a6c6-564258762746?title=Pivot%20table%20-%20Wikipedia"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        A pivot table is a way to create contingency tables using spreadsheet software.
      </li>
      <li>
        &#21487;&#20197;&#20351;&#29992;pd.pivot_table, pd.groupby&#26469;&#23454;&#29616;&#12290;&#21518;&#32773;&#20284;&#20046;&#21482;&#33021;&#22312;&#19968;&#20010;&#36724;&#19978;&#32858;&#21512;&#65292;&#21069;&#32773;&#21487;&#20197;&#21516;&#26102;&#22312;&#20004;&#20010;&#36724;&#19978;&#32858;&#21512;&#12290;&#21442;&#35265;&#12298;&#21033;&#29992;Python&#36827;&#34892;&#25968;&#25454;&#20998;&#26512;&#12299;p288&#19978;&#30340;&#20363;&#23376;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Contingency Table" ID="ID_688207321" CREATED="1541149088299" MODIFIED="1541150375564" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/64dd0031-f65f-4e74-9e04-2b74caa79d91?title=Contingency%20table%20-%20Wikipedia"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21363;crosstab&#65292;&#29992;&#26469;&#35745;&#31639;&#20004;&#20010;&#21464;&#37327;&#21516;&#26102;&#20986;&#29616;&#30340;&#39057;&#29575;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x4f7f;&#x7528;Pandas&#x521b;&#x5efa;&#x8fd9;&#x4e9b;&#x8868;" ID="ID_1732165131" CREATED="1541149188250" MODIFIED="1541150481616"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        &#21487;&#20197;&#20351;&#29992;pd.groupby, pd.crosstab&#25110;&#32773;pd.pivot_table

        <ul>
          <li>
            pd.groupby&#30340;&#21151;&#33021;&#20284;&#20046;&#21482;&#33021;&#22788;&#29702;&#19968;&#20010;&#36724;&#19978;&#30340;&#32858;&#21512;
          </li>
        </ul>
      </li>
      <li>
        &#20004;&#20010;&#26041;&#27861;&#37117;&#21487;&#20197;&#25351;&#23450;&#32858;&#21512;&#20989;&#25968;&#65292;&#21442;&#35265;https://app.yinxiang.com/shard/s25/nl/6691581/7a58f244-fb91-4b68-bd0e-dffeaab4c7f3?title=how%20to%20use%20two%20different%20functions%20within%20crosstab%2Fpivot_table%20in%20pandas%3F&#12290;
      </li>
      <li>
        &#21442;&#35265;&#12298;&#21033;&#29992;Python&#36827;&#34892;&#25968;&#25454;&#20998;&#26512;&#12299;ch9&#8220;&#36879;&#35270;&#34920;&#21644;&#20132;&#21449;&#34920;&#8221;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Tools" ID="ID_1331691503" CREATED="1541304393942" MODIFIED="1541304400294">
<node TEXT="tpdm" LOCALIZED_STYLE_REF="styles.topic" ID="ID_132782078" CREATED="1541255569075" MODIFIED="1541515472445"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#19968;&#20010;&#26041;&#20415;&#20351;&#29992;&#30340;&#36827;&#24230;&#26465;python&#21253;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="matplotlib" LOCALIZED_STYLE_REF="styles.topic" ID="ID_533177245" CREATED="1541304402275" MODIFIED="1541515477612">
<node TEXT="&#x8bbe;&#x7f6e;label&#x548c;stick&#x7684;&#x989c;&#x8272;" ID="ID_1820812379" CREATED="1541304449323" MODIFIED="1541305357396"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#22240;&#20026;&#22312;dark&#20027;&#39064;&#19979;&#65292;&#40664;&#35748;&#30340;&#40657;&#33394;&#23383;&#20307;&#30475;&#19981;&#28165;&#26970;&#12290;&#21442;&#32771;&#22914;&#19979;&#22238;&#31572;
    </p>
    <ul>
      <li>
        &#36825;&#20010;&#26041;&#27861;&#38656;&#35201;&#27599;&#27425;&#35774;&#32622;: https://stackoverflow.com/questions/4761623/changing-the-color-of-the-axis-ticks-and-labels-for-a-plot-in-matplotlib
      </li>
      <li>
        &#36825;&#20010;&#26041;&#27861;&#26159;&#23450;&#21046;matplotlib: plt.style.use(['dark_background'])&#65292;&#21442;&#35265;https://matplotlib.org/users/customizing.html
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node TEXT="&#x66f4;&#x591a;&#x53ef;&#x7528;&#x7684;&#x4e3b;&#x9898;" ID="ID_1852156905" CREATED="1541484396284" MODIFIED="1541484419824" LINK="https://matplotlib.org/gallery/style_sheets/style_sheets_reference.html"/>
</node>
</node>
</node>
<node TEXT="&#x8fdb;&#x4e00;&#x6b65;&#x63d0;&#x5347;" ID="ID_1263556004" CREATED="1541256543930" MODIFIED="1541256587997" LINK="https://www.kaggle.com/kashnitsky/correct-time-aware-cross-validation-scheme"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20316;&#32773;&#30340;kernel&#65292;&#23545;&#22914;&#20309;&#36827;&#19968;&#27493;&#25552;&#21319;&#30340;&#31034;&#33539;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_764507078" CREATED="1541988407055" MODIFIED="1541988482474"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      <b>Assignment 6</b>
    </p>
    <p>
      
    </p>
    <p>
      https://mlcourse.ai/assignments
    </p>
    <p>
      Beating baselines in the competition &quot;How good is your Medium article?&quot;
    </p>
    <p>
      https://www.kaggle.com/c/how-good-is-your-medium-article
    </p>
    <p>
      
    </p>
    <p>
      This time it's freeride-only, you need to beat the &quot;A6 baseline&quot; on Public LB and rename your team in accordance with our rating. *No need to upload* your code (you'll need to do it only in the end of the course, check https://mlcourse.ai/roadmap, sections on competitions)
    </p>
    <p>
      
    </p>
    <p>
      Kernels to help:
    </p>
    <p>
      -&#160;&#160;https://www.kaggle.com/kashnitsky/ridge-countvectorizer-baseline - a simple baseline
    </p>
    <p>
      - https://www.kaggle.com/jandevel/clap-it-exploratory-data-analysis - nice EDA
    </p>
    <p>
      - https://www.kaggle.com/kashnitsky/ridge-and-lightgbm-simple-blending - a simple blending scheme
    </p>
    <p>
      
    </p>
    <p>
      You're welcome to share some insights and upvote each other Kernels. But pls don't spoil the idea of competition: do not share high-performing code (with MAE &lt; 1.5 for Medium and AUC &gt; 0.95 for Alice).
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="assignment 6" ID="ID_1532272070" CREATED="1542440237950" MODIFIED="1542440241666">
<node TEXT="HTMLParser" LOCALIZED_STYLE_REF="styles.topic" ID="ID_110958884" CREATED="1542440243675" MODIFIED="1542442263721" LINK="https://docs.python.org/3/library/html.parser.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23448;&#26041;&#30340;&#31616;&#20171;&#27604;&#36739;&#22909;&#25026;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Topics" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_485342921" CREATED="1540893955562" MODIFIED="1541516294676">
<edge COLOR="#ff0000"/>
<node TEXT="*Week 3*&#xa;&#xa;- Material on basics of Machine Learning, decision trees and kNN https://mlcourse.ai/notebooks/blob/master/jupyter_english/topic03_decision_trees_kNN/topic3_decision_trees_kNN.ipynb&#xa;- Assignment 3 https://mlcourse.ai/notebooks/blob/master/jupyter_english/assignments_fall2018/assignment3_decision_trees.ipynb?flush_cache=true. Deadline: *October 28, 21:59 UTC+2*&#xa;- Lecture 3 will be there next weekend&#xa;- As for kNN, you can take an optional assignment from cs231n. It&apos;s about an efficient vectorized implementation of kNN for image classification http://cs231n.github.io/assignments2018/assignment1/ (part 1). It&apos;s perfect if you plan to further study neural nets. The assignment is in principle simple (as is kNN itself), however vectorization is a bit challenging&#xa;- Optional assignment on implementing decision trees is on it&apos;s way, will be published in a couple of days (edited)" FOLDED="true" ID="ID_1856246051" CREATED="1542006734163" MODIFIED="1542006739411">
<node TEXT="*Week 3 updates*&#xa;&#xa;*New lectures*&#xa;Lecture 3 goes in 2 parts: theory and practice. First, we cover basic ideas in Machine Learning, then switch to particular supervised learning tasks, and cover decision trees in detail. There are many important aspects covered, including cross-validation and model selection. https://youtu.be/H4XlBTPv5rQ&#xa;&#xa;In the second part, we use Sklearn to train, tune, and visualize decision trees.&#xa;https://youtu.be/RrVYO6Td9Js&#xa;&#xa;*Update on A3*&#xa;This is not critical, only that regression splitting criterion is now better explained in the very beginning. Do a git pull or just check out here https://mlcourse.ai/notebooks/blob/master/jupyter_english/assignments_fall2018/assignment3_decision_trees.ipynb&#xa;A reminder: stick to threads when working on assignments, this thread for a3_q1 https://opendatascience.slack.com/archives/C91N8TL83/p1539692684000100 and there are 4 more.&#xa;&#xa;*Solution to a demo version of A3*&#xa;Available as a Kaggle Kernel. In covers classification with a toy dataset (as opposed to a A3 (not demo) where you&apos;ll delve into basics of regression trees), and then with a real dataset. https://www.kaggle.com/kashnitsky/a3-demo-decision-trees-solution  It&apos;s been there for quite a while (3 days), just making public thing &quot;more public&quot; again. Feel free to upvote the kernel if you found it useful :slightly_smiling_face: Together, a demo assignment and a full version of A3 will provide a nice picture of how decision trees work for both classification and regression. And I&apos;ll remind you: trees are here, there and everywhere. Not purely, but inside Random Forest and gradient boosting.&#xa;&#xa;For now, I&apos;m almost not available till next Friday. Ph.D. defense. Feel free to address @datamove @ecdrid and @maximkeremet With Sunday (A2 solution and announcing A4) everything will go as planned. (edited)" ID="ID_1711501896" CREATED="1542006691420" MODIFIED="1542006699911"/>
</node>
<node TEXT="4. Linear Regression" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1425274536" CREATED="1540978987526" MODIFIED="1541732871366" HGAP_QUANTITY="26.7499996200204 pt" VSHIFT_QUANTITY="17.249999485909953 pt">
<node TEXT="Part3" FOLDED="true" ID="ID_260067658" CREATED="1541337607471" MODIFIED="1541337614431">
<node TEXT="Regularization" ID="ID_1084819640" CREATED="1541333845308" MODIFIED="1541333906429">
<node TEXT="sklearn" ID="ID_1762156080" CREATED="1541333938705" MODIFIED="1541333971339"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        C&#36234;&#22823;&#65292;&#23545;&#26435;&#20540;&#30340;&#24809;&#32602;&#36234;&#23567;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Feature Engine" ID="ID_921078946" CREATED="1541337429092" MODIFIED="1541337439385">
<node TEXT="&#x6dfb;&#x52a0;&#x591a;&#x9879;&#x5f0f;&#x9ad8;&#x9636;&#x56e0;&#x5b50;" ID="ID_1041343172" CREATED="1541337440331" MODIFIED="1541337580644"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      from sklearn.preprocessing import PolynomialFeatures
    </p>
    <p>
      poly = PolynomialFeatures(degree=3)
    </p>
    <p>
      X_poly = poly.fit_transform(X)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="5. Ensembles and random foreset" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1806881420" CREATED="1541409333745" MODIFIED="1542006844294" HGAP_QUANTITY="13.250000022351742 pt" VSHIFT_QUANTITY="26.99999919533732 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      *Week 5. Ensembles and Random Forests.*
    </p>
    <p>
      
    </p>
    <p>
      - Medium article https://goo.gl/S7jUWz :clap: :heavy_multiplication_x: 50 as usual
    </p>
    <p>
      - In the fifth assignment https://goo.gl/bUL3ad, you&#8217;ll compare Random Forest and logistic regression in the credit scoring task. You&#8217;ll understand pros and cons of both approaches.&#160;&#160;Hard deadline: March 18, 23:59 CET.
    </p>
    <p>
      
    </p>
    <p>
      Assignment #6 will be announced on March, 19, although topic 6 will be written on March, 12 as it was planned. We&#8217;ll have the same Assignment #6 in :uk: and :ru: so we have to synchronize with our Russian version of the course. So maybe you have free time :slightly_smiling_face: So it&#8217;s high time to join Kaggle Inclass competitions. (edited)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Part1" ID="ID_560710956" CREATED="1541409428832" MODIFIED="1541409452164"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Bagging
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Ensemble" ID="ID_704503681" CREATED="1541411146862" MODIFIED="1541411152626">
<node TEXT="Condorcet&apos;s jury theorem" ID="ID_1606388291" CREATED="1541411058965" MODIFIED="1541411120912" LINK="https://en.wikipedia.org/wiki/Condorcet%27s_jury_theorem">
<node TEXT="\latex \mu = \sum^{N}_{i=m}{N \choose i}p^i(1-p)^{N-i}, m = \frac{N + 1}{2}" ID="ID_567570767" CREATED="1541929221283" MODIFIED="1541932316885"/>
</node>
<node TEXT="Wisdom of the crowd" ID="ID_771768148" CREATED="1541411373131" MODIFIED="1541411388500" LINK="https://en.wikipedia.org/wiki/Wisdom_of_the_crowd"/>
</node>
<node TEXT="Bootstrapping" ID="ID_986664088" CREATED="1541411558954" MODIFIED="1541411566352">
<node TEXT="Bootstrap aggregation" ID="ID_1928671597" CREATED="1541411568082" MODIFIED="1541411655534" LINK="https://en.wikipedia.org/wiki/Bootstrap_aggregating"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Bagging is also known as bootstrap aggregation
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Bootstrapping" ID="ID_1646274119" CREATED="1541411587409" MODIFIED="1541411677388" LINK="https://en.wikipedia.org/wiki/Bootstrapping_%28statistics%29"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Bagging is based on it.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x7528;Churn&#x8fd9;&#x4e2a;&#x6570;&#x636e;&#x96c6;&#x793a;&#x4f8b;" ID="ID_574687340" CREATED="1541413061207" MODIFIED="1541413089919">
<node TEXT="numpy" LOCALIZED_STYLE_REF="styles.topic" ID="ID_470457086" CREATED="1541413091271" MODIFIED="1541515417393">
<node TEXT="np.percentile" ID="ID_1923596825" CREATED="1541413099335" MODIFIED="1541413192235"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      a = np.array([1, 2, 3, 4, 5])
    </p>
    <p>
      np.percentile(a, [0, 25, 50, 75, 100])
    </p>
    <p>
      Out[39]: array([1., 2., 3., 4., 5.])
    </p>
    <p>
      &#21487;&#20197;&#21457;&#29616;&#65292;0&#23545;&#24212;&#31532;&#19968;&#20010;&#25968;&#65292;100&#23545;&#24212;&#26368;&#21518;&#19968;&#20010;&#25968;&#65292;&#20013;&#38388;&#31561;&#38388;&#36317;&#30340;&#23545;&#24212;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x4ece;Loyal&#x548c;Churn&#x7684;&#x5206;&#x5e03;&#x66f2;&#x7ebf;&#x53ef;&#x4ee5;&#x5927;&#x81f4;&#x770b;&#x51fa;&#x4e8c;&#x8005;&#x7684;&#x533a;&#x522b;&#xff0c;&#x4f46;&#x5982;&#x4f55;&#x7edf;&#x8ba1;&#x7684;&#x63cf;&#x8ff0;&#x8fd9;&#x4e2a;&#x5dee;&#x522b;&#x5462;?&#x6700;&#x7b80;&#x5355;&#x7684;&#x65b9;&#x6cd5;&#x5c31;&#x662f;&#x5747;&#x503c;&#x3002;&#x4f46;&#x662f;&#x7531;&#x4e8e;&#x6570;&#x636e;&#x91cf;&#x5c0f;&#xff0c;&#x5747;&#x503c;&#x548c;&#x671f;&#x671b;&#x7684;&#x5dee;&#x522b;&#x53ef;&#x80fd;&#x6bd4;&#x8f83;&#x5927;&#x3002;&#x8bfe;&#x7a0b;&#x4e2d;&#x7684;&#x4ee3;&#x7801;&#x5c31;&#x6f14;&#x793a;&#x4e86;&#x5982;&#x4f55;&#x5229;&#x7528;Bagging&#x6280;&#x672f;&#xff0c;&#x4f30;&#x7b97;&#x5747;&#x503c;&#xff0c;&#x5e76;&#x7ed9;&#x51fa;&#x7f6e;&#x4fe1;&#x5ea6;&#x3002;" ID="ID_549800453" CREATED="1541413321510" MODIFIED="1541413434925"/>
</node>
</node>
<node TEXT="&#x6ce8;&#x610f;&#xff0c;Bagging&#x5bf9;bias-variance&#x7684;&#x964d;&#x4f4e;&#x5b58;&#x5728;&#x4e00;&#x4e2a;&#x524d;&#x63d0;&#x5047;&#x8bbe;: &#x5355;&#x4e2a;&#x5b66;&#x4e60;&#x673a;&#x5668;&#x7684;&#x9519;&#x8bef;&#x662f;&#x4e0d;&#x76f8;&#x5173;&#x7684;&#xff0c;&#x4f46;&#x8fd9;&#x5f88;&#x591a;&#x65f6;&#x5019;&#x5728;&#x73b0;&#x5b9e;&#x4e16;&#x754c;&#x5e76;&#x4e0d;&#x6210;&#x7acb;&#x3002;" ID="ID_1464793331" CREATED="1541426618428" MODIFIED="1541426696990"/>
<node TEXT="scikit-learn" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1777774212" CREATED="1541426174350" MODIFIED="1541515371237">
<node TEXT="BaggingRegressor&#x548c;BaggingClassifier&#x505a;&#x57fa;&#x7c7b;" ID="ID_849445469" CREATED="1541426184831" MODIFIED="1541426224363"/>
<node TEXT="Single estimator vs. Bagging: bias-variance decomposition" ID="ID_943383919" CREATED="1541426379180" MODIFIED="1541426420733" LINK="http://scikit-learn.org/stable/auto_examples/ensemble/plot_bias_variance.html#sphx-glr-auto-examples-ensemble-plot-bias-variance-py"/>
</node>
<node TEXT="Out of Bags&#x90e8;&#x5206;&#x4e0d;&#x592a;&#x7406;&#x89e3;" ID="ID_1957274342" CREATED="1541428798089" MODIFIED="1541428889066">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="Part2" ID="ID_1396082814" CREATED="1541428994135" MODIFIED="1541515718337"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Random Forest
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Leo Breiman managed to apply bootstrapping not only in statistics but also in machine learning. He, along with Adel Cutler, extended and improved the random forest algorithm proposed by Tin Kam Ho. They combined the construction of uncorrelated trees using CART, bagging, and the random subspace method." ID="ID_1517379382" CREATED="1541429026302" MODIFIED="1541429053743">
<node TEXT="&#x7b2c;&#x4e00;&#x7bc7;&#x63d0;&#x51fa;&#x968f;&#x673a;&#x68ee;&#x6797;&#x7684;&#x8bba;&#x6587;" ID="ID_576857495" CREATED="1541429189359" MODIFIED="1541429213816" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/eb966da1-674f-48f8-b00a-1c8a98d7e1c6/"/>
<node TEXT="CART" ID="ID_1980260184" CREATED="1541429136979" MODIFIED="1541429145331" LINK="https://en.wikipedia.org/wiki/Predictive_analytics#Classification_and_regression_trees_.28CART.29"/>
<node TEXT="Random Subspace Method" ID="ID_337328518" CREATED="1541429162664" MODIFIED="1541429284966" LINK="https://en.wikipedia.org/wiki/Random_subspace_method"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;The random subspace method reduces the correlation between the trees and thus prevents overfitting
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="You can see random forest as bagging of decision trees with the modification of selecting a random subset of features at each split." ID="ID_1016808651" CREATED="1541482162288" MODIFIED="1541515718335" HGAP_QUANTITY="13.250000022351742 pt" VSHIFT_QUANTITY="-0.7499999776482589 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#31639;&#27861;&#25551;&#36848;&#22312;&#26412;&#37096;&#20998;&#31532;&#19968;&#33410;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x5173;&#x4e8e;RF&#x7684;variance&#x548c;bias&#xff0c;&#x6ca1;&#x6709;&#x770b;&#x5f97;&#x592a;&#x61c2;&#x3002;&#x597d;&#x50cf;&#x5927;&#x4f53;&#x662f;&#x8bf4;&#xff0c;RF&#x4e3b;&#x8981;&#x662f;&#x901a;&#x8fc7;&#x964d;&#x4f4e;variance&#x6765;&#x83b7;&#x5f97;&#x68c0;&#x6d4b;&#x7387;&#x7684;&#x6539;&#x8fdb;&#x7684;&#x3002;" ID="ID_667509635" CREATED="1541925718361" MODIFIED="1541925778280"/>
<node TEXT="RF&#x548c;K&#x8fd1;&#x90bb;&#x7684;&#x76f8;&#x4f3c;&#x6027;" ID="ID_1410463201" CREATED="1541926740493" MODIFIED="1541926767678"/>
<node TEXT="RF&#x4e5f;&#x53ef;&#x4ee5;&#x7528;&#x5728;&#x65e0;&#x76d1;&#x7763;" ID="ID_1795171311" CREATED="1541926843356" MODIFIED="1541926879781"/>
</node>
<node TEXT="Part3" ID="ID_1413441693" CREATED="1541429019142" MODIFIED="1541927227126"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Feature Importance
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_298816867" CREATED="1541928414400" MODIFIED="1541928451532"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      sklearn&#30340;&#38543;&#26426;&#26862;&#26519;&#20855;&#26377;&#19968;&#20010;&#23646;&#24615;&#21483;&#20570;<a charset="utf-8" class="reference internal" href="https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier.feature_importances_" title="sklearn.ensemble.RandomForestClassifier.feature_importances_" style="color: rgb(5, 87, 129); text-decoration: underline; font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 700; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)"><font color="black" face="monospace" size="1.1em"><u><b><code class="xref py py-obj docutils literal" style="padding-top: 2px; padding-bottom: 2px; padding-right: 4px; padding-left: 4px; font-family: monospace; font-size: 1.1em; color: black; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; white-space: nowrap; font-weight: bold"><span class="pre">feature_importances_</span></code></b></u></font></a>&#65292;&#21487;&#20197;&#36820;&#22238;&#29305;&#24449;&#30340;&#37325;&#35201;&#24615;&#65292;&#20540;&#36234;&#39640;&#36234;&#37325;&#35201;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="6. Feature Engine" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1141496677" CREATED="1542007170938" MODIFIED="1542028089832"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      *Week 6*
    </p>
    <p>
      
    </p>
    <p>
      - Paper #6 is about feature engineering and feature selection https://goo.gl/rFjTjZ
    </p>
    <p>
      - In assignment #6 you are challenged to beat two more serious benchmarks in 2 competitions &#8211; Alice and Medium
    </p>
    <p>
      - Solution to assignment #4 https://goo.gl/1Y8UYi will live for 2-3 days (edited)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x9ad8;&#x8d28;&#x91cf;&#x7684;&#x6570;&#x636e;&#x52a0;&#x4e0a;&#x7b80;&#x5355;&#x7684;&#x6a21;&#x578b;&#x5f80;&#x5f80;&#x80dc;&#x8fc7;&#x7cdf;&#x7cd5;&#x7684;&#x6570;&#x636e;&#x52a0;&#x4e0a;&#x590d;&#x6742;&#x7684;&#x6a21;&#x578b;" ID="ID_1919919019" CREATED="1542027923175" MODIFIED="1542027995386"/>
<node TEXT="Feature Extraction and Feature Engine" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_1417320643" CREATED="1542028040657" MODIFIED="1542031132668"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        &#23558;&#29305;&#24449;&#36716;&#25442;&#20026;&#27169;&#22411;&#21487;&#20197;&#25110;&#32773;&#26131;&#20110;&#22788;&#29702;&#30340;&#24418;&#24335;&#12290;&#20363;&#22914;&#25991;&#26412;&#30340;&#31232;&#30095;&#30690;&#37327;&#21270;
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node TEXT="Text" FOLDED="true" ID="ID_185654704" CREATED="1542030603354" MODIFIED="1542030605093">
<node TEXT="Stemming and lemmatization" ID="ID_1850961567" CREATED="1542030619770" MODIFIED="1542030636776" LINK="https://nlp.stanford.edu/IR-book/html/htmledition/stemming-and-lemmatization-1.html"/>
<node TEXT="sklearn.feature_extraction.text" FOLDED="true" ID="ID_1225473644" CREATED="1542031001259" MODIFIED="1542451107918" LINK="https://scikit-learn.org/stable/modules/feature_extraction.html#text-feature-extraction">
<node TEXT="CountVectorize" FOLDED="true" ID="ID_123897125" CREATED="1542031015323" MODIFIED="1542031020469">
<node TEXT="N-grams" ID="ID_1797587126" CREATED="1542031032466" MODIFIED="1542031090117"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23558;&#21333;&#35789;&#30340;&#27425;&#24207;&#32771;&#34385;&#36827;&#21435;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="TfidfVectorize" ID="ID_1228476853" CREATED="1542445833399" MODIFIED="1542463697228"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#30456;&#24403;&#20110;CountVectorize + TfidfTransformer
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="TF-IDF" ID="ID_1623045926" CREATED="1542031305441" MODIFIED="1542031318741" LINK="https://en.wikipedia.org/wiki/Tf%E2%80%93idf"/>
<node ID="ID_597354618" CREATED="1542031668115" MODIFIED="1542031673041"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span charset="utf-8" style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="black" face="-apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="14px">Analogs of Bag of Words can be found outside of text problems e.g. bag of sites in the</font></span><font color="black" face="-apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="14px"><span>&#160;</span><a href="https://inclass.kaggle.com/c/catch-me-if-you-can-intruder-detection-through-webpage-session-tracking" rel="nofollow" target="_blank" style="text-decoration: none; color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">Catch Me If You Can competition</a><span style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none">,</span><span>&#160;</span><a href="https://www.kaggle.com/xiaoml/talkingdata-mobile-user-demographics/bag-of-app-id-python-2-27392" rel="nofollow" target="_blank" style="text-decoration: none; color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">bag of apps</a><span style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none">,</span><span>&#160;</span><a href="http://www.interdigital.com/download/58540a46e3b9659c9f000372" rel="nofollow" target="_blank" style="text-decoration: none; color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">bag of events</a></font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="The most popular method in the new wave is Word2Vec, but there are a few alternatives as well (GloVe, Fasttext, etc.)." FOLDED="true" ID="ID_1984981952" CREATED="1542031793978" MODIFIED="1542031795884">
<node TEXT="Word2Vec is a special case of the word embedding algorithms. Using Word2Vec and similar models, we can not only vectorize words in a high-dimensional space (typically a few hundred dimensions) but also compare their semantic similarity." ID="ID_1616567727" CREATED="1542031850010" MODIFIED="1542031852018"/>
<node ID="ID_1236819454" CREATED="1542032054280" MODIFIED="1542032059369"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span charset="utf-8" style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="black" face="-apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="14px">Such models need to be trained on very large datasets in order for the vector coordinates to capture the semantics. A pretrained model for your own tasks can be downloaded</font></span><font color="black" face="-apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="14px"><span>&#160;</span><a href="https://github.com/3Top/word2vec-api#where-to-get-a-pretrained-models" rel="nofollow" target="_blank" style="text-decoration: none; color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">here</a><span style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none">.</span></font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_700423086" CREATED="1542032110280" MODIFIED="1542032128691"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span charset="utf-8" style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="black" face="-apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="14px">Similar methods are applied in other areas such as bioinformatics. An unexpected application is</font></span><font color="black" face="-apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol" size="14px"><span>&#160;</span><a href="https://jaan.io/food2vec-augmented-cooking-machine-intelligence/" rel="nofollow" target="_blank" style="text-decoration: none; color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">food2vec</a><span style="color: black; font-family: -apple-system, system-ui, Segoe UI, Helvetica, Arial, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol; font-size: 14px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none">.</span></font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Word2Vec" ID="ID_1502359213" CREATED="1542447710286" MODIFIED="1542447715311" LINK="https://radimrehurek.com/gensim/models/word2vec.html"/>
<node TEXT="glove" ID="ID_1948311932" CREATED="1542447798433" MODIFIED="1542447801707" LINK="https://github.com/JonathanRaiman/glove"/>
</node>
</node>
<node TEXT="Image" ID="ID_1588928394" CREATED="1542032616322" MODIFIED="1542032888474"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#22270;&#20687;&#21644;&#22768;&#38899;&#30340;&#24059;&#31215;&#36816;&#31639;&#30475;&#19978;&#21435;&#20855;&#26377;&#26222;&#36941;&#24615;&#65292;&#20294;&#25105;&#35273;&#24471;&#20381;&#28982;&#26159;&#29305;&#23450;&#39046;&#22495;&#30340;&#12290;&#22270;&#20687;&#30340;&#31354;&#38388;&#30456;&#20851;&#24615;&#65292;&#22768;&#38899;&#30340;&#26102;&#38388;&#30456;&#20851;&#24615;&#65292;&#22312;&#25968;&#25454;&#31185;&#23398;&#20013;&#24182;&#19981;&#24635;&#26159;&#23384;&#22312;&#12290;&#20363;&#22914;&#20851;&#20110;&#25151;&#20215;&#39044;&#27979;&#30340;&#29305;&#24449;&#20013;&#65292;&#19981;&#21516;&#29305;&#24449;&#30340;&#25490;&#24067;&#39034;&#24207;&#24182;&#19981;&#24433;&#21709;&#20160;&#20040;&#65292;&#20294;&#26159;&#22270;&#20687;&#21644;&#22768;&#38899;&#30340;&#25968;&#25454;&#26159;&#19981;&#33021;&#38543;&#24847;&#35843;&#25442;&#39034;&#24207;&#30340;&#12290;
    </p>
    <p>
      
    </p>
    <p>
      (&#25968;&#32452;&#12289;&#30690;&#37327;&#12289;&#24352;&#37327;&#26159;&#20160;&#20040;&#20851;&#31995;? &#20160;&#20040;&#26159;&#32500;&#24230;?)
    </p>
    <p>
      
    </p>
    <p>
      &#36825;&#20123;&#20063;&#21551;&#31034;&#29305;&#24449;&#24037;&#31243;&#30340;&#37325;&#35201;&#24615;(&#20381;&#28982;&#20855;&#26377;&#21551;&#21457;&#24335;&#30340;&#29305;&#28857;)&#65292;&#24212;&#35813;&#27880;&#24847;&#24605;&#32771;&#19981;&#21516;&#39046;&#22495;&#29305;&#24449;&#30340;&#24322;&#21516;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Geospatial" FOLDED="true" ID="ID_1214943126" CREATED="1542032780388" MODIFIED="1542032786901">
<node TEXT="geopy" ID="ID_1737633425" CREATED="1542094112850" MODIFIED="1542094147462" LINK="https://github.com/geopy/geopy"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#19968;&#20010;Python&#21253;&#65292;&#23553;&#35013;&#20102;Google Map&#31561;&#25509;&#21475;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Google Map" FOLDED="true" ID="ID_711934716" CREATED="1542095389911" MODIFIED="1542095392590">
<node TEXT="&#x8c03;&#x7528;&#x6b21;&#x6570;&#x6709;&#x9650;&#x5236;" ID="ID_231802152" CREATED="1542095543032" MODIFIED="1542095547056"/>
</node>
<node TEXT="OpenStreetMap" FOLDED="true" ID="ID_752594423" CREATED="1542095393297" MODIFIED="1542095396828">
<node TEXT="&#x6709;&#x672c;&#x5730;&#x7248;&#x63a5;&#x53e3;&#xff0c;&#x4f46;&#x529f;&#x80fd;&#x6709;&#x9650;" ID="ID_1859306172" CREATED="1542095528929" MODIFIED="1542095541587"/>
</node>
</node>
<node TEXT="Date and Time" FOLDED="true" ID="ID_265799867" CREATED="1542101199724" MODIFIED="1542101202295">
<node TEXT="&#x628a;&#x5c0f;&#x65f6;&#x8f6c;&#x6362;&#x5230;&#x5706;&#x76d8;&#x4e0a;&#x6765;&#x89e3;&#x51b3;&#x65f6;&#x95f4;&#x8ddd;&#x79bb;&#x5ea6;&#x91cf;&#x7684;&#x95ee;&#x9898;" ID="ID_1144564412" CREATED="1542101891199" MODIFIED="1542101912212"/>
</node>
<node TEXT="Time Series" FOLDED="true" ID="ID_1988543879" CREATED="1542101202823" MODIFIED="1542101205409">
<node TEXT="tsfresh" ID="ID_362392055" CREATED="1542101261979" MODIFIED="1542101314612" LINK="https://github.com/blue-yonder/tsfresh"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      python&#24211;: &#33258;&#21160;&#20174;&#26102;&#38388;&#24207;&#21015;&#20013;&#25552;&#21462;&#29305;&#24449;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Web Data" FOLDED="true" ID="ID_859998953" CREATED="1542101565921" MODIFIED="1542101568049">
<node TEXT="python module: user_agents" ID="ID_1449517371" CREATED="1542101568705" MODIFIED="1542101593031"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21487;&#20197;&#35299;&#26512;&#27983;&#35272;&#22120;&#20449;&#24687;&#22836;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Feature Transformation" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_1519896936" CREATED="1542028055574" MODIFIED="1542028085272">
<node ID="ID_1068713081" CREATED="1542102607163" MODIFIED="1542102922139"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21407;&#22240;
    </p>
    <ul>
      <li>
        &#21333;&#35843;&#30340;&#29305;&#24449;&#36716;&#25442;&#23545;&#26576;&#20123;&#31639;&#27861;&#26377;&#25928;&#65292;&#26377;&#20123;&#27809;&#26377;&#12290;
      </li>
      <li>
        &#26377;&#26102;&#20351;&#29992;log&#26469;&#22788;&#29702;&#36807;&#22823;&#30340;&#25968;&#20540;&#65292;&#36991;&#20813;&#36229;&#20986;&#33539;&#22260;
      </li>
      <li>
        &#26377;&#26102;&#26159;&#21442;&#25968;&#21270;&#31639;&#27861;&#30340;&#35201;&#27714;&#12290;&#20363;&#23376;?
      </li>
      <li>
        &#36991;&#20813;&#29305;&#24449;&#30340;&#20540;&#22495;&#27604;&#20363;&#22833;&#35843;&#65292;&#26377;&#20123;&#20540;&#20998;&#24067;&#33539;&#22260;&#24456;&#23567;&#65292;&#26377;&#20123;&#24456;&#22823;&#12290;&#20363;&#22914;&#23545;&#20110;KNN&#65292;&#36825;&#26679;&#30340;&#38382;&#39064;&#23601;&#20250;&#23548;&#33268;&#31639;&#27861;&#22833;&#25928;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x4f8b;&#x5982;StandardScalar, MinMaxScalar&#x3002;&#x4e24;&#x8005;&#x4e00;&#x822c;&#x5dee;&#x4e0d;&#x591a;&#xff0c;&#x4f46;&#x662f;&#x5982;&#x679c;&#x6d89;&#x53ca;&#x8ddd;&#x79bb;&#x5ea6;&#x91cf;&#xff0c;&#x5219;&#x4f7f;&#x7528;&#x524d;&#x8005;&#x3002;&#x800c;&#x540e;&#x8005;&#x5728;&#x53ef;&#x89c6;&#x5316;&#x7684;&#x65f6;&#x5019;&#x6bd4;&#x8f83;&#x9002;&#x5408;&#x3002;" ID="ID_97731346" CREATED="1542103042920" MODIFIED="1542103109123"/>
<node TEXT="&#x5bf9;&#x6570;&#x6b63;&#x6001;&#x5206;&#x5e03;" FOLDED="true" ID="ID_1123829939" CREATED="1542103221664" MODIFIED="1542103642430" LINK="https://en.wikipedia.org/wiki/Log-normal_distribution"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#27604;&#36739;&#36866;&#21512;&#25551;&#36848;&#24037;&#36164;&#65292;&#20154;&#24037;&#65292;&#25991;&#31456;&#30340;&#35780;&#35770;&#25968;&#31561;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x5982;&#x679c;&#x77e5;&#x9053;&#x6570;&#x636e;&#x662f;&#x5bf9;&#x6570;&#x6b63;&#x6001;&#x5206;&#x5e03;&#xff0c;&#x53ef;&#x4ee5;&#x901a;&#x8fc7;np.log&#x5c06;&#x6570;&#x636e;&#x8f6c;&#x6362;&#xff0c;&#x4f7f;&#x5f97;&#x6570;&#x636e;&#x7b26;&#x5408;&#x6b63;&#x6001;&#x5206;&#x5e03;" FOLDED="true" ID="ID_1621288158" CREATED="1542104071634" MODIFIED="1542104142627"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#36825;&#20010;&#26041;&#27861;&#19981;&#26159;&#19968;&#23450;&#35201;&#27714;&#25968;&#25454;&#26159;&#23545;&#25968;&#27491;&#24577;&#20998;&#24067;&#30340;&#65292;&#20063;&#36866;&#21512;&#19968;&#33324;&#30340;&#38271;&#23614;&#25968;&#25454;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x6216;&#x8005;&#x4e5f;&#x53ef;&#x4ee5;&#x8bd5;&#x8bd5;np.log(x + const)" ID="ID_634950051" CREATED="1542104288977" MODIFIED="1542104302487"/>
</node>
<node TEXT="boxcox" FOLDED="true" ID="ID_90565352" CREATED="1542103904428" MODIFIED="1542104265508" LINK="https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.boxcox.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23545;&#25968;&#36716;&#25442;&#21487;&#20197;&#30475;&#20316;&#26159;&#20854;&#29305;&#20363;&#12290;&#36890;&#36807;boxcox&#21487;&#20197;&#23558;&#25968;&#25454;&#36716;&#25442;&#20026;&#25509;&#36817;&#27491;&#24577;&#20998;&#24067;&#12290;&#24456;&#22810;&#31639;&#27861;&#37117;&#20551;&#35774;&#25968;&#25454;&#26159;&#27491;&#24577;&#20998;&#24067;&#30340;&#65292;&#25152;&#20197;&#36825;&#26679;&#30340;&#36716;&#25442;&#21487;&#20197;&#20351;&#24471;&#35813;&#31639;&#27861;&#26356;&#21152;&#36866;&#21512;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Yeo-Johnson transformation" ID="ID_314581119" CREATED="1542103940243" MODIFIED="1542103979102" LINK="https://gist.github.com/mesgarpour/f24769cd186e2db853957b10ff6b7a95"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23545;boxcox&#30340;&#25193;&#23637;&#65292;&#20351;&#24471;&#21487;&#20197;&#22788;&#29702;&#36127;&#25968;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Q-Q plot" FOLDED="true" ID="ID_354138219" CREATED="1542103644837" MODIFIED="1542103652477" LINK="https://en.wikipedia.org/wiki/Q%E2%80%93Q_plot">
<node TEXT="statsmodels" ID="ID_840104064" CREATED="1542104386409" MODIFIED="1542104399559" LINK="https://www.statsmodels.org/stable/index.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      python&#24211;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="missing data" FOLDED="true" ID="ID_1782843096" CREATED="1542185744276" MODIFIED="1542185747710">
<node TEXT="pd.fillna" ID="ID_705338932" CREATED="1542185748369" MODIFIED="1542185758728"/>
<node TEXT="sklearn.preprocessing.Imputer" ID="ID_671504256" CREATED="1542185759402" MODIFIED="1542185772647"/>
</node>
</node>
<node TEXT="Feature Selection" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_1599133844" CREATED="1542028068103" MODIFIED="1542028086616">
<node ID="ID_1613194618" CREATED="1542185932657" MODIFIED="1542185963629"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20026;&#20160;&#20040;&#38656;&#35201;&#29305;&#24449;&#36873;&#25321;?
    </p>
    <ul>
      <li>
        &#20943;&#23569;&#35745;&#31639;&#37327;
      </li>
      <li>
        &#26377;&#20123;&#27169;&#22411;&#23545;&#22122;&#22768;&#25935;&#24863;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x7edf;&#x8ba1;&#x7684;&#x65b9;&#x6cd5;" FOLDED="true" ID="ID_419911548" CREATED="1542186354797" MODIFIED="1542186367255">
<node TEXT="&#x53bb;&#x6389;variance&#x4f4e;&#x4e8e;&#x67d0;&#x4e2a;&#x95e8;&#x9650;&#x7684;&#x6570;&#x636e;&#x3002;&#x80cc;&#x540e;&#x7684;&#x4f9d;&#x636e;&#x662f;&#xff0c;&#x63d0;&#x4f9b;&#x7684;&#x4fe1;&#x606f;&#x91cf;&#x592a;&#x5c11;&#xff0c;&#x6781;&#x7aef;&#x7684;&#x5c31;&#x662f;&#x5e38;&#x91cf;&#x3002;" FOLDED="true" ID="ID_1640501255" CREATED="1542185996488" MODIFIED="1542186032508">
<node TEXT="sklearn.feature_selection.VarianceThreshold" ID="ID_1590688084" CREATED="1542186069527" MODIFIED="1542186088224"/>
</node>
<node TEXT="sklearn.feature_selection.SelectKBest, f_classify" ID="ID_763792403" CREATED="1542186311444" MODIFIED="1542186342677"/>
</node>
<node TEXT="&#x57fa;&#x4e8e;&#x6a21;&#x578b;&#x7684;&#x9009;&#x62e9;" FOLDED="true" ID="ID_404228730" CREATED="1542186421387" MODIFIED="1542186466425"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#32972;&#21518;&#30340;&#20551;&#35774;: &#22914;&#26524;&#19968;&#20010;&#29305;&#24449;&#22312;&#31616;&#21333;&#30340;&#27169;&#22411;&#20013;&#27809;&#26377;&#22810;&#22823;&#29992;&#22788;&#65292;&#20063;&#23601;&#27809;&#26377;&#24517;&#35201;&#28155;&#21152;&#21040;&#22797;&#26434;&#30340;&#27169;&#22411;&#20013;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="sklearn.feature_selection.SelectFromModel" FOLDED="true" ID="ID_894892316" CREATED="1542186849999" MODIFIED="1542186861434">
<node TEXT="" ID="ID_1290018233" CREATED="1542186978237" MODIFIED="1542186986791">
<hook URI="../../Desktop/select_frm_model.jpg" SIZE="0.9174312" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="grid search" FOLDED="true" ID="ID_1668803083" CREATED="1542187086509" MODIFIED="1542187207037"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      exhaustive feature selection
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Exhaustive Feature Selector" ID="ID_1821155958" CREATED="1542188905057" MODIFIED="1542188970770" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/ef714f96-d1cb-4bbb-b9e2-560fe5c76a96?title=Exhaustive%20Feature%20Selector%20-%20mlxtend"/>
<node TEXT="Sequential Feature Selector" FOLDED="true" ID="ID_600182126" CREATED="1542188912393" MODIFIED="1542188980436" LINK="https://app.yinxiang.com/shard/s25/nl/6691581/99c17fbd-17d0-476b-a061-55c108e61f5b?title=Sequential%20Feature%20Selector%20-%20mlxtend">
<node TEXT="sklearn.feature_selection.SequentialFeatureSelector" ID="ID_403783218" CREATED="1542189169654" MODIFIED="1542189190835"/>
<node TEXT="a example" ID="ID_609927221" CREATED="1542189421332" MODIFIED="1542189426054" LINK="https://www.kaggle.com/arsenyinfo/easy-feature-selection-pipeline-0-55-at-lb"/>
</node>
</node>
</node>
<node TEXT="Regression" FOLDED="true" ID="ID_684536464" CREATED="1542191015380" MODIFIED="1542191018432">
<node TEXT="Lasso Regression" FOLDED="true" ID="ID_129444859" CREATED="1542190865613" MODIFIED="1542190907784"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#35268;&#25972;&#21270;&#22240;&#23376;&#20351;&#29992;||x||_1
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x4ece;&#x6587;&#x7ae0;&#x7684;&#x5b9e;&#x9a8c;&#x6765;&#x770b;&#xff0c;&#x770b;&#x4e0a;&#x53bb;&#x9002;&#x5408;&#x7528;&#x5728;&#x7279;&#x5f81;&#x9009;&#x62e9;" ID="ID_1802586758" CREATED="1542190925341" MODIFIED="1542191002752"/>
<node TEXT="sklearn: Lasso, LassoCV" ID="ID_571955054" CREATED="1542191062027" MODIFIED="1542191072281"/>
</node>
<node TEXT="Ridge Regression" FOLDED="true" ID="ID_1038803394" CREATED="1542190871845" MODIFIED="1542190921885"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#35268;&#25972;&#21270;&#22240;&#23376;&#20351;&#29992;||x||_2
    </p>
  </body>
</html>
</richcontent>
<node TEXT="sklearn: Ridge, RidgeCV" ID="ID_42729200" CREATED="1542191074812" MODIFIED="1542191081363"/>
</node>
<node TEXT="Generalize Linear Regression" ID="ID_979424427" CREATED="1542191025491" MODIFIED="1542191038853" LINK="https://scikit-learn.org/stable/modules/linear_model.html"/>
</node>
</node>
<node TEXT="7. Unsupervised Learning and dimension reduction" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_695029620" CREATED="1542006629309" MODIFIED="1543146989720"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      This time, it'll be something completely different - unsupervised learning. We'll cover 2 tasks where you get value from unlabeled data, namely, dimensionality reduction with PCA (Principal Component Analysis), and clustering.
    </p>
    <p>
      
    </p>
    <p>
      *Article* - https://bit.ly/2z3jxrw
    </p>
    <p>
      *Assignment* - https://bit.ly/2qI11Re (deadline is Nov. 25, 23.59 UTC+2), use pinned threads&#160;&#160;*#a7_q1*, ... *#a7_q9* for your questions.
    </p>
    <p>
      
    </p>
    <p>
      Also, the second half of project &quot;Alice&quot; is now released https://github.com/Yorko/mlcourse.ai/tree/master/jupyter_english/project_alice (edited)
    </p>
  </body>
</html>

</richcontent>
<node TEXT="PCA" ID="ID_1047583870" CREATED="1543147075970" MODIFIED="1543147080899">
<node TEXT="sklearn.decompostion.PCA" ID="ID_495130791" CREATED="1543147082559" MODIFIED="1543147110433"/>
</node>
<node TEXT="T-SNE" ID="ID_565050897" CREATED="1543147165405" MODIFIED="1543147167791">
<node TEXT="sklearn.manifold.TSNE" ID="ID_1653609591" CREATED="1543147168197" MODIFIED="1543147184222"/>
</node>
<node TEXT="distance" ID="ID_778976188" CREATED="1543147261243" MODIFIED="1543147263421">
<node TEXT="scipy.spatial.distance.cdist" ID="ID_1908037512" CREATED="1543147264147" MODIFIED="1543147281821"/>
<node TEXT="scipy.spatial.distance.pdist" ID="ID_1317327412" CREATED="1543147351513" MODIFIED="1543147357579"/>
</node>
<node TEXT="cluster" ID="ID_740670070" CREATED="1543147295410" MODIFIED="1543147296964">
<node TEXT="sklearn.cluster" ID="ID_1885745602" CREATED="1543147297314" MODIFIED="1543147420002">
<node TEXT="KMeans" ID="ID_487878691" CREATED="1543147382985" MODIFIED="1543147385482"/>
<node TEXT="AgglomerativeClustering" ID="ID_1281505930" CREATED="1543147386112" MODIFIED="1543147388139"/>
<node TEXT="AffinityPropagation" ID="ID_1314764021" CREATED="1543147395088" MODIFIED="1543147398595"/>
<node TEXT="SpectralClustering" ID="ID_1315349061" CREATED="1543147412216" MODIFIED="1543147413698"/>
</node>
<node TEXT="scipy.cluster.hierarchy" ID="ID_47515405" CREATED="1543147325170" MODIFIED="1543147342396"/>
</node>
</node>
</node>
<node TEXT="Projects" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_655984947" CREATED="1541409164737" MODIFIED="1541598626871">
<edge COLOR="#cccc00"/>
<node TEXT="Alice" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1149057109" CREATED="1541409171107" MODIFIED="1541516498308">
<node TEXT="Logistic Regression" ID="ID_719908914" CREATED="1541409179138" MODIFIED="1541409189133">
<node ID="ID_1011673591" CREATED="1541409191602" MODIFIED="1541409307123"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#22914;&#20309;&#26500;&#24314;&#22909;&#30340;&#29305;&#24449;?
    </p>
    <ul>
      <li>
        &#24182;&#38750;&#25152;&#26377;&#28155;&#21152;&#30340;&#29305;&#24449;&#37117;&#20250;&#24102;&#26469;&#22686;&#30410;
      </li>
      <li>
        &#20160;&#20040;&#26679;&#30340;&#29305;&#24449;&#20250;&#24102;&#26469;&#36127;&#22686;&#30410;?
      </li>
      <li>
        &#20160;&#20040;&#26679;&#30340;&#29305;&#24449;&#20250;&#24102;&#26469;&#27491;&#30340;&#22686;&#30410;?
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="Libraries" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_50267779" CREATED="1541513824880" MODIFIED="1541598562144" HGAP_QUANTITY="55.999998748302495 pt" VSHIFT_QUANTITY="69.74999792128807 pt">
<edge COLOR="#009933"/>
<node TEXT="scikit-learn" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1599290778" CREATED="1541513829952" MODIFIED="1541516130704">
<node LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_744255808" CREATED="1541513857128" MODIFIED="1541670772800" LINK="http://scikit-learn.org/stable/glossary.html#term-n-jobs"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      Glossary
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20171;&#32461;&#20102;Scikit-Learn&#20013;&#30340;&#27010;&#24565;&#65292;&#29305;&#21035;&#26159;&#23545;&#20110;&#26041;&#27861;&#21644;&#21442;&#25968;&#37117;&#29992;&#19968;&#33268;&#30340;&#21629;&#21517;&#35268;&#33539;&#65292;&#20102;&#35299;&#19968;&#19979;&#23545;&#20110;&#29702;&#35299;&#20854;&#25509;&#21475;&#20250;&#24102;&#26469;&#19968;&#20123;&#26041;&#20415;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Guide" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_378105202" CREATED="1541597718796" MODIFIED="1541597857079">
<node TEXT="Supervised Learning" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1373182715" CREATED="1541837069957" MODIFIED="1541837096618" LINK="https://scikit-learn.org/stable/supervised_learning.html">
<node TEXT="Ensemble" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_212505123" CREATED="1541837083197" MODIFIED="1541837100802">
<node TEXT="Random Forest" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_201294370" CREATED="1541837088381" MODIFIED="1541837103113"/>
</node>
</node>
<node TEXT="Model Seleting and Evaluation" LOCALIZED_STYLE_REF="styles.topic" ID="ID_622663784" CREATED="1541597915891" MODIFIED="1541598041859">
<node TEXT="Cross Validation" LOCALIZED_STYLE_REF="styles.subtopic" FOLDED="true" ID="ID_903020785" CREATED="1541599573592" MODIFIED="1541832622020" LINK="https://scikit-learn.org/stable/modules/cross_validation.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#22312;&#35757;&#32451;&#38598;&#19978;&#23398;&#20064;&#65292;&#21448;&#22312;&#35757;&#32451;&#38598;&#19978;&#27979;&#35797;&#65292;&#26159;&#19968;&#31181;&#26041;&#27861;&#24615;&#38169;&#35823;&#65292;&#20250;&#20986;&#29616;&#36807;&#25311;&#21512;&#30340;&#38382;&#39064;&#65292;&#25152;&#20197;&#38656;&#35201;&#20174;&#35757;&#32451;&#38598;&#20013;&#25286;&#20998;&#20986;&#27979;&#35797;&#38598;&#12290;
    </p>
    <p>
      
    </p>
    <p>
      &#27169;&#22411;&#25110;&#32773;&#35757;&#32451;&#36807;&#31243;&#24448;&#24448;&#36824;&#26377;&#19968;&#20123;<b>&#36229;&#21442;&#25968;</b>&#65292;&#20363;&#22914;&#35268;&#25972;&#21270;&#22240;&#23376;&#65292;&#22914;&#26524;&#30452;&#25509;&#22312;&#27979;&#35797;&#38598;&#19978;&#35843;&#25972;&#36825;&#20123;&#36229;&#21442;&#25968;&#65292;&#21017;&#30456;&#24403;&#20110;&#20851;&#20110;&#27979;&#35797;&#38598;&#30340;&#30693;&#35782;&#20197;&#36229;&#21442;&#25968;&#30340;&#24418;&#24335;&#28431;&#36827;(leak)&#20102;&#27169;&#22411;&#37324;&#65292;&#36825;&#26679;&#65292;&#24230;&#37327;&#25351;&#26631;&#23601;&#19981;&#33021;&#23458;&#35266;&#30340;&#34913;&#37327;&#27169;&#22411;&#30340;&#27867;&#21270;&#27700;&#24179;&#12290;&#25152;&#20197;&#65292;&#36824;&#38656;&#35201;&#20998;&#20986;&#19968;&#20010;<b>&#26657;&#39564;&#38598;</b>&#12290;&#20294;&#26159;&#65292;&#20998;&#20986;&#26469;&#19977;&#20010;&#38598;&#21512;&#65292;&#19968;&#26041;&#38754;&#26174;&#33879;&#30340;&#38477;&#20302;&#20102;&#21487;&#29992;&#20110;&#23398;&#20064;&#30340;&#25968;&#25454;&#65292;&#21516;&#26102;&#21448;&#20351;&#24471;&#32467;&#26524;&#20381;&#36182;&#20110;&#26576;&#20010;&#29305;&#23450;&#30340;&#25968;&#25454;&#21010;&#20998;&#12290;&#19968;&#31181;&#35299;&#20915;&#36825;&#20123;&#38382;&#39064;&#30340;&#21150;&#27861;&#23601;&#26159;<b>&#20132;&#21449;&#39564;&#35777;</b>&#65292;&#21363;&#27979;&#35797;&#38598;&#36824;&#26159;&#21333;&#29420;&#25319;&#20986;&#26469;&#65292;&#20294;&#19981;&#20877;&#21306;&#20998;&#26657;&#39564;&#38598;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Interfaces" LOCALIZED_STYLE_REF="styles.subsubtopic" ID="ID_1234478687" CREATED="1541598937666" MODIFIED="1541645952537" HGAP_QUANTITY="69.49999834597116 pt" VSHIFT_QUANTITY="-14.999999552965178 pt">
<node TEXT="train_test_split" LOCALIZED_STYLE_REF="default" ID="ID_1586418163" CREATED="1541598753627" MODIFIED="1541645967468"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23558;&#25968;&#25454;&#38598;&#25353;&#32473;&#23450;&#27604;&#20363;&#38543;&#26426;&#20998;&#25104;&#35757;&#32451;&#38598;&#21644;&#27979;&#35797;&#38598;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="cross_val_score" FOLDED="true" ID="ID_1618752611" CREATED="1541515139817" MODIFIED="1541600858388"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20856;&#22411;&#30340;&#29992;&#27861;&#26159;cross_val_score(estimator, X, y, cv=7). &#36825;&#37324;cv=7&#23601;&#26159;&#35828;7 fold&#12290;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="from sklearn.model_selection import cross_val_score&#xa;clf = svm.SVC(kernel=&apos;linear&apos;, C=1)&#xa;scores = cross_val_score(clf, iris.data, iris.target, cv=5)&#xa;print(&quot;Accuracy: %0.2f (+/- %0.2f)&quot; % (scores.mean(), scores.std() * 2))" ID="ID_1264863474" CREATED="1541600971584" MODIFIED="1541600978599"/>
<node ID="ID_1323578055" CREATED="1541601041806" MODIFIED="1541672482188"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      When the <i>cv</i>&#160;argument is an integer, <i>cross_val_score</i>&#160;uses the <u>KFold</u>&#160;&#160;or <u>StratifiedKFold</u>&#160;strategies by default, the latter being used if the estimator derives from <u>ClassifierMixin</u>.
    </p>
  </body>
</html>
</richcontent>
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_1730293497" STARTINCLINATION="623;0;" ENDINCLINATION="623;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node ID="ID_671717500" CREATED="1541601207105" MODIFIED="1541601268744"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <i>cv</i>&#20063;&#21487;&#20197;&#26159;&#19968;&#20010;&#23545;&#35937;&#65292;&#29992;&#20110;&#21010;&#20998;&#35757;&#32451;&#38598;&#65292;&#20363;&#22914;
    </p>
    <p>
      
    </p>
    <p>
      from sklearn.model_selection import ShuffleSplit
    </p>
    <p>
      n_samples = iris.data.shape[0]
    </p>
    <p>
      cv = ShuffleSplit(n_splits=5, test_size=0.3, random_state=0)
    </p>
    <p>
      cross_val_score(clf, iris.data, iris.target, cv=cv)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="score&#x7684;&#x8ba1;&#x7b97;&#x65b9;&#x6cd5;&#x662f;&#x53ef;&#x4ee5;&#x6307;&#x5b9a;&#x7684;" ID="ID_1235805607" CREATED="1541600862835" MODIFIED="1541600883315" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter"/>
</node>
<node TEXT="cross_validate" ID="ID_1406068861" CREATED="1541671228228" MODIFIED="1541671286346"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21644;cross_val_score&#30340;&#19981;&#21516;&#22312;&#20110;&#65292;&#25903;&#25345;&#25351;&#23450;&#22810;&#20010;&#24230;&#37327;&#65292;&#24182;&#36820;&#22238;&#22810;&#20010;&#24230;&#37327;&#30340;&#20998;&#25968;&#12290;&#25152;&#35859;&#22810;&#20010;&#24230;&#37327;&#65292;&#20363;&#22914;precision_score, recall_score&#31561;&#31561;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Cross Validation Iterator" LOCALIZED_STYLE_REF="styles.subsubtopic" FOLDED="true" ID="ID_999228045" CREATED="1541671455411" MODIFIED="1541732679055">
<node TEXT="i.i.d Data" ID="ID_974823085" CREATED="1541672482018" MODIFIED="1541672783128">
<font BOLD="true"/>
<node TEXT="k-fold CV" LOCALIZED_STYLE_REF="default" FOLDED="true" ID="ID_1730293497" CREATED="1541599861890" MODIFIED="1541671563459" LINK="https://scikit-learn.org/stable/modules/cross_validation.html#k-fold"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23558;&#35757;&#32451;&#38598;&#20998;&#25104;k&#20010;&#23567;&#38598;&#21512;&#65292;&#27599;&#27425;&#35757;&#32451;&#25361;&#36873;&#20854;&#20013;k-1&#20010;&#38598;&#21512;&#20316;&#20026;&#35757;&#32451;&#38598;&#65292;&#21097;&#19979;&#30340;&#20316;&#20026;&#26657;&#39564;&#38598;&#12290;&#26368;&#32456;&#30340;&#24230;&#37327;&#25351;&#26631;&#26159;&#20960;&#27425;&#23454;&#39564;&#30340;&#24179;&#22343;&#12290;
    </p>
    <p>
      
    </p>
    <p>
      &#36825;&#31181;&#26041;&#27861;&#27604;&#36739;&#28040;&#32791;&#35745;&#31639;&#65292;&#20294;&#26159;&#36866;&#21512;&#23567;&#25968;&#37327;&#30340;&#24773;&#20917;&#12290;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="import numpy as np&#xa;from sklearn.model_selection import KFold&#xa;&#xa;X = [&quot;a&quot;, &quot;b&quot;, &quot;c&quot;, &quot;d&quot;]&#xa;kf = KFold(n_splits=2)&#xa;for train, test in kf.split(X):&#xa;    print(&quot;%s %s&quot; % (train, test))" ID="ID_1852858462" CREATED="1541671593968" MODIFIED="1541671599296"/>
</node>
<node TEXT="Repeated k-fold" ID="ID_638963800" CREATED="1541671794069" MODIFIED="1541671841821"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#37325;&#22797;&#22810;&#27425;&#30340;k-fold&#65292;&#27599;&#19968;&#27425;&#30340;&#21010;&#20998;&#37117;&#19981;&#21516;&#12290;&#36825;&#21482;&#26159;k-fold&#30340;&#19968;&#31181;&#26041;&#20415;&#23553;&#35013;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Leave One Out" ID="ID_370550991" CREATED="1541671881165" MODIFIED="1541671985101"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#26159;&#19968;&#31181;&#29305;&#27530;&#30340;k-fold&#65292;&#21363;k=N&#65292;N&#26159;&#26679;&#26412;&#30340;&#25968;&#37327;&#65292;&#21363;&#27599;&#27425;&#36339;&#20986;&#19968;&#20010;&#26679;&#26412;&#20316;&#20026;&#27979;&#35797;&#38598;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Leave P Out" ID="ID_708338157" CREATED="1541672002812" MODIFIED="1541672102400"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21644;leave one out&#30456;&#20284;&#65292;&#21482;&#26159;&#20174;1&#21464;&#25104;&#20102;P
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Shuffle Split" FOLDED="true" ID="ID_278806481" CREATED="1541672137898" MODIFIED="1541672295441" LINK="https://scikit-learn.org/stable/modules/cross_validation.html#random-permutations-cross-validation-a-k-a-shuffle-split"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#38543;&#26426;&#29983;&#25104;&#35757;&#32451;&#38598;&#21644;&#27979;&#35797;&#38598;&#65292;&#20854;&#20013;&#27979;&#35797;&#38598;&#30340;&#23481;&#37327;&#22823;&#23567;&#30001;&#29992;&#25143;&#25351;&#23450;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&gt;&gt;&gt; from sklearn.model_selection import ShuffleSplit&#xa;&gt;&gt;&gt; X = np.arange(10)&#xa;&gt;&gt;&gt; ss = ShuffleSplit(n_splits=5, test_size=0.25,&#xa;...     random_state=0)&#xa;&gt;&gt;&gt; for train_index, test_index in ss.split(X):&#xa;...     print(&quot;%s %s&quot; % (train_index, test_index))&#xa;[9 1 6 7 3 0 5] [2 8 4]&#xa;[2 9 8 0 6 7 4] [3 5 1]&#xa;[4 5 1 0 6 9 7] [2 3 8]&#xa;[2 7 5 8 0 3 4] [6 1 9]&#xa;[4 1 0 6 8 9 3] [5 2 7]" ID="ID_1261230312" CREATED="1541672282112" MODIFIED="1541672286237"/>
</node>
</node>
<node TEXT="Imbalance distribution of Data" ID="ID_26878180" CREATED="1541672600590" MODIFIED="1541672784451">
<font BOLD="true"/>
<node TEXT="Stratified k-fold" FOLDED="true" ID="ID_523611393" CREATED="1541672611149" MODIFIED="1541728120372"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#22312;&#21010;&#20998;&#35757;&#32451;&#38598;&#21644;&#27979;&#35797;&#38598;&#30340;&#26102;&#20505;&#65292;&#20250;&#22312;&#35757;&#32451;&#38598;&#21644;&#27979;&#35797;&#38598;&#20013;&#37117;&#23613;&#37327;&#20445;&#30041;&#26679;&#26412;&#31867;&#21035;&#30340;&#27604;&#20363;&#20851;&#31995;&#19981;&#21464;
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_157705699" CREATED="1541672727876" MODIFIED="1541672752143"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre style="margin-top: 0; line-height: 1.2em; white-space: pre-wrap; font-style: normal; font-size: 13px; padding-top: 5px; margin-bottom: 0; font-family: Monaco, Menlo, Consolas, Courier New, monospace; margin-left: 0px; text-indent: 0px; padding-bottom: 5px; word-spacing: 0px; padding-left: 10px; color: rgb(34, 34, 34); background-color: rgb(248, 248, 248); margin-right: 0px; display: block; letter-spacing: normal; font-weight: 400; padding-right: 10px; text-align: start; text-transform: none" content="text/html; charset=utf-8" http-equiv="content-type"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">from</font></b> <font color="rgb(14, 132, 181)"><b>sklearn.model_selection</b></font> <font color="rgb(0, 112, 32)"><b>import</b></font> StratifiedKFold
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>X <font color="rgb(102, 102, 102)">=</font> np<font color="rgb(102, 102, 102)">.</font>ones(<font color="rgb(32, 128, 80)">10</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>y <font color="rgb(102, 102, 102)">=</font> [<font color="rgb(32, 128, 80)">0</font>, <font color="rgb(32, 128, 80)">0</font>, <font color="rgb(32, 128, 80)">0</font>, <font color="rgb(32, 128, 80)">0</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>]
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>skf <font color="rgb(102, 102, 102)">=</font> StratifiedKFold(n_splits<font color="rgb(102, 102, 102)">=</font><font color="rgb(32, 128, 80)">3</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">for</font></b> train, test <font color="rgb(0, 112, 32)"><b>in</b></font> skf<font color="rgb(102, 102, 102)">.</font>split(X, y):
<font color="rgb(198, 93, 9)"><b>... </b></font>    <font color="rgb(0, 112, 32)">print</font>(<font color="rgb(64, 112, 160)">&quot;</font><i><font color="rgb(112, 160, 208)">%s</font></i><font color="rgb(64, 112, 160)"> </font><i><font color="rgb(112, 160, 208)">%s</font></i><font color="rgb(64, 112, 160)">&quot;</font> <font color="rgb(102, 102, 102)">%</font> (train, test))
<font color="rgb(51, 51, 51)">[2 3 6 7 8 9] [0 1 4 5]</font>
<font color="rgb(51, 51, 51)">[0 1 3 4 5 8 9] [2 6 7]</font>
<font color="rgb(51, 51, 51)">[0 1 2 4 5 6 7] [3 8 9]</font></pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Repeated Stratified k-fold" ID="ID_1967103787" CREATED="1541727826232" MODIFIED="1541727837211"/>
<node TEXT="Stratified Shuffle Split" ID="ID_479808019" CREATED="1541727842395" MODIFIED="1541727957853"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#31867;&#20284;Shuffle Split&#65292;&#19981;&#21516;&#30340;&#26159;&#20445;&#30041;&#31867;&#21035;&#27604;&#20363;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Grouped Data" FOLDED="true" ID="ID_238618892" CREATED="1541729028526" MODIFIED="1541729462799"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#19968;&#20010;&#20363;&#23376;&#23601;&#26159;&#21307;&#23398;&#25968;&#25454;&#65292;&#26469;&#33258;&#19981;&#21516;&#30149;&#20154;&#30340;&#25968;&#25454;&#23601;&#32452;&#25104;&#19981;&#21516;&#30340;&#32452;&#65292;&#30149;&#20154;ID&#23601;&#26159;&#32452;ID&#12290;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="Group K-Fold" FOLDED="true" ID="ID_1829882972" CREATED="1541729483556" MODIFIED="1541729661371"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#30456;&#27604;K-Fold&#65292;&#24046;&#21035;&#26159;
    </p>
    <ul>
      <li>
        K&#19981;&#33021;&#22823;&#20110;&#32452;&#30340;&#20010;&#25968;
      </li>
      <li>
        &#20445;&#35777;&#21516;&#19968;&#20010;&#32452;&#30340;&#25968;&#25454;&#19981;&#20250;&#21516;&#26102;&#20986;&#29616;&#22312;&#35757;&#32451;&#38598;&#21644;&#27979;&#35797;&#38598;&#20013;
      </li>
      <li>
        &#19981;&#21516;&#32452;&#30340;&#25968;&#25454;&#20250;&#20998;&#21035;&#20986;&#29616;&#22312;&#19981;&#21516;&#20998;&#21106;&#37324;&#30340;&#27979;&#35797;&#38598;&#20013;&#65292;&#19981;&#22810;&#20063;&#19981;&#23569;
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node ID="ID_1921202199" CREATED="1541729921633" MODIFIED="1541729949608"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre style="margin-right: 0px; text-transform: none; font-family: Monaco, Menlo, Consolas, Courier New, monospace; color: rgb(34, 34, 34); text-align: start; font-style: normal; margin-bottom: 0; padding-top: 5px; white-space: pre-wrap; padding-bottom: 5px; background-color: rgb(248, 248, 248); font-size: 13px; padding-right: 10px; padding-left: 10px; font-weight: 400; display: block; word-spacing: 0px; letter-spacing: normal; line-height: 1.2em; margin-top: 0; text-indent: 0px; margin-left: 0px" content="text/html; charset=utf-8" http-equiv="content-type"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">from</font></b> <font color="rgb(14, 132, 181)"><b>sklearn.model_selection</b></font> <font color="rgb(0, 112, 32)"><b>import</b></font> GroupKFold
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>X <font color="rgb(102, 102, 102)">=</font> [<font color="rgb(32, 128, 80)">0.1</font>, <font color="rgb(32, 128, 80)">0.2</font>, <font color="rgb(32, 128, 80)">2.2</font>, <font color="rgb(32, 128, 80)">2.4</font>, <font color="rgb(32, 128, 80)">2.3</font>, <font color="rgb(32, 128, 80)">4.55</font>, <font color="rgb(32, 128, 80)">5.8</font>, <font color="rgb(32, 128, 80)">8.8</font>, <font color="rgb(32, 128, 80)">9</font>, <font color="rgb(32, 128, 80)">10</font>]
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>y <font color="rgb(102, 102, 102)">=</font> [<font color="rgb(64, 112, 160)">&quot;a&quot;</font>, <font color="rgb(64, 112, 160)">&quot;b&quot;</font>, <font color="rgb(64, 112, 160)">&quot;b&quot;</font>, <font color="rgb(64, 112, 160)">&quot;b&quot;</font>, <font color="rgb(64, 112, 160)">&quot;c&quot;</font>, <font color="rgb(64, 112, 160)">&quot;c&quot;</font>, <font color="rgb(64, 112, 160)">&quot;c&quot;</font>, <font color="rgb(64, 112, 160)">&quot;d&quot;</font>, <font color="rgb(64, 112, 160)">&quot;d&quot;</font>, <font color="rgb(64, 112, 160)">&quot;d&quot;</font>]
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>groups <font color="rgb(102, 102, 102)">=</font> [<font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">1</font>, <font color="rgb(32, 128, 80)">2</font>, <font color="rgb(32, 128, 80)">2</font>, <font color="rgb(32, 128, 80)">2</font>, <font color="rgb(32, 128, 80)">3</font>, <font color="rgb(32, 128, 80)">3</font>, <font color="rgb(32, 128, 80)">3</font>, <font color="rgb(32, 128, 80)">3</font>]
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font>gkf <font color="rgb(102, 102, 102)">=</font> GroupKFold(n_splits<font color="rgb(102, 102, 102)">=</font><font color="rgb(32, 128, 80)">3</font>)
<font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font><b><font color="rgb(0, 112, 32)">for</font></b> train, test <font color="rgb(0, 112, 32)"><b>in</b></font> gkf<font color="rgb(102, 102, 102)">.</font>split(X, y, groups<font color="rgb(102, 102, 102)">=</font>groups):
<font color="rgb(198, 93, 9)"><b>... </b></font>    <font color="rgb(0, 112, 32)">print</font>(<font color="rgb(64, 112, 160)">&quot;</font><font color="rgb(112, 160, 208)"><i>%s</i></font><font color="rgb(64, 112, 160)"> </font><font color="rgb(112, 160, 208)"><i>%s</i></font><font color="rgb(64, 112, 160)">&quot;</font> <font color="rgb(102, 102, 102)">%</font> (train, test))
<font color="rgb(51, 51, 51)">[0 1 2 3 4 5] [6 7 8 9]</font>
<font color="rgb(51, 51, 51)">[0 1 2 6 7 8 9] [3 4 5]</font>
<font color="rgb(51, 51, 51)">[3 4 5 6 7 8 9] [0 1 2]</font></pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Leave One Group Out" ID="ID_1189996118" CREATED="1541730147833" MODIFIED="1541730175592"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#27599;&#27425;&#25361;&#36873;&#19968;&#32452;&#20316;&#20026;&#27979;&#35797;&#38598;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Leave P Group Out" ID="ID_260903526" CREATED="1541730202838" MODIFIED="1541730242724"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#31867;&#20284;Leave One Group Out&#65292;&#21482;&#26159;One&#21464;&#25104;&#20102;&#21487;&#20197;&#35774;&#32622;&#30340;&#21464;&#37327;P
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Group Shuffle Split" ID="ID_828277388" CREATED="1541730298632" MODIFIED="1541730368038"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#31867;&#20284;Shuffle Split&#21644;Leave P Group out&#30340;&#32452;&#21512;&#65292;&#22312;group&#20013;&#36827;&#34892;&#38543;&#26426;&#32452;&#21512;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Time Series Data" FOLDED="true" ID="ID_353332402" CREATED="1541730437031" MODIFIED="1541730628046"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#26102;&#38388;&#24207;&#21015;&#25968;&#25454;&#20551;&#35774;&#26102;&#38388;&#19978;&#25509;&#36817;&#30340;&#25968;&#25454;&#26159;&#30456;&#20851;&#30340;&#65292;&#32780;KFold&#21644;ShuffleSplit&#26159;&#20551;&#35774;&#25968;&#25454;&#26159;i.i.d.&#30340;&#65292;&#36825;&#21487;&#33021;&#23548;&#33268;&#35757;&#32451;&#38598;&#21644;&#27979;&#35797;&#38598;&#20043;&#38388;&#30340;&#30456;&#20851;&#24615;&#12290;
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
<node TEXT="TimeSeriesSplit" FOLDED="true" ID="ID_523161751" CREATED="1541730616782" MODIFIED="1541732279884"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20855;&#26377;&#22914;&#19979;&#29305;&#28857;
    </p>
    <ul>
      <li>
        &#23558;&#21069;k&#20010;fold&#20316;&#20026;&#35757;&#32451;&#38598;&#65292;&#31532;k+1&#20010;&#20316;&#20026;&#27979;&#35797;&#38598;
      </li>
      <li>
        K&#19981;&#33021;&#22823;&#20110;N-1&#65292;&#20854;&#20013;N&#26159;&#19981;&#21516;&#26102;&#38388;&#28857;&#30340;&#20010;&#25968;
      </li>
      <li>
        &#27599;&#19968;&#27425;&#26032;&#30340;&#21010;&#20998;&#65292;&#37117;&#27604;&#21069;&#19968;&#27425;&#30340;&#35757;&#32451;&#38598;&#22810;&#19968;&#20010;&#26102;&#38388;&#28857;&#30340;&#25968;&#25454;
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node ID="ID_292948886" CREATED="1541731993680" MODIFIED="1541732308016"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre style="margin-right: 0px; text-transform: none; font-family: Monaco, Menlo, Consolas, Courier New, monospace; color: rgb(34, 34, 34); text-align: start; font-style: normal; margin-bottom: 0; padding-top: 5px; white-space: pre-wrap; padding-bottom: 5px; background-color: rgb(248, 248, 248); font-size: 13px; padding-right: 10px; padding-left: 10px; font-weight: 400; display: block; word-spacing: 0px; letter-spacing: normal; line-height: 1.2em; margin-top: 0; text-indent: 0px; margin-left: 0px" content="text/html; charset=utf-8" http-equiv="content-type"><span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.model_selection</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">TimeSeriesSplit</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">X</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">np</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">array</span><span class="p">([[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">2</font></span><span class="p">],</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">3</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">4</font></span><span class="p">],</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">2</font></span><span class="p">],</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">3</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">4</font></span><span class="p">],</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">2</font></span><span class="p">],</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">3</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">4</font></span><span class="p">]])</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">y</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">np</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">array</span><span class="p">([</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">2</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">3</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">4</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">5</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">6</font></span><span class="p">])</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">tscv</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">TimeSeriesSplit</span><span class="p">(</span><span class="n">n_splits</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">3</font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="nb" style="color: rgb(0, 112, 32)"><font color="rgb(0, 112, 32)">print</font></span><span class="p">(</span><span class="n">tscv</span><span class="p">)</span>  
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">TimeSeriesSplit(max_train_size=None, n_splits=3)</font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">for</font></span></b> <span class="n">train</span><span class="p">,</span> <span class="n">test</span> <span class="ow" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>in</b></font></span> <span class="n">tscv</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">split</span><span class="p">(</span><span class="n">X</span><span class="p">):</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span class="nb" style="color: rgb(0, 112, 32)"><font color="rgb(0, 112, 32)">print</font></span><span class="p">(</span><span class="s2" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">&quot;</font></span><span class="si" style="color: rgb(112, 160, 208); font-style: italic"><font color="rgb(112, 160, 208)"><i>%s</i></font></span><span class="s2" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)"> </font></span><span class="si" style="color: rgb(112, 160, 208); font-style: italic"><font color="rgb(112, 160, 208)"><i>%s</i></font></span><span class="s2" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">&quot;</font></span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">%</font></span> <span class="p">(</span><span class="n">train</span><span class="p">,</span> <span class="n">test</span><span class="p">))</span>
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">[0 1 2] [3]</font></span>
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">[0 1 2 3] [4]</font></span>
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">[0 1 2 3 4] [5]</font></span></pre>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        n_splits&#19981;&#33021;&#22823;&#20110;5
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="PredefinedSplit" ID="ID_661946825" CREATED="1541730398359" MODIFIED="1541730646129"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25163;&#21160;&#30340;&#36827;&#34892;&#21010;&#20998;&#25110;&#32773;&#35828;&#33258;&#23450;&#20041;&#21010;&#20998;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="About Shuffle" ID="ID_1436477384" CREATED="1541732686309" MODIFIED="1541732702441"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: left">
      &#22914;&#26524;&#25968;&#25454;&#26159;i.i.d.&#30340;&#65292;&#37027;&#20040;shuffle&#25968;&#25454;&#26159;&#24517;&#35201;&#30340;&#12290;&#20294;&#22914;&#26524;&#25968;&#25454;&#26412;&#36523;&#20855;&#26377;&#19968;&#23450;&#30340;&#27425;&#24207;&#65292;&#20363;&#22914;&#26102;&#38388;&#25968;&#25454;&#65292;shuffle&#23601;&#19981;&#22826;&#22909;&#20102;&#65292;&#20250;&#23548;&#33268;&#36807;&#25311;&#21512;&#21644;&#39564;&#35777;&#38598;&#20998;&#25968;&#21464;&#22823;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Tuning Hyper Parameters" LOCALIZED_STYLE_REF="styles.subtopic" FOLDED="true" ID="ID_994362581" CREATED="1541732805380" MODIFIED="1541832606033" LINK="https://scikit-learn.org/stable/modules/grid_search.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        &#20856;&#22411;&#30340;&#36229;&#21442;&#25968;&#22914;&#35268;&#25972;&#21270;&#22240;&#23376;C&#65292;SVM&#20013;&#30340;kernel&#21644;gamma&#65292;Lasso&#37324;&#30340;alpha&#31561;&#31561;&#12290;
      </li>
      <li>
        &#36229;&#21442;&#25968;&#24314;&#35758;&#21033;&#29992;&#20132;&#21449;&#39564;&#35777;&#36827;&#34892;&#35843;&#21442;&#12290;
      </li>
      <li>
        estimator.get_params()&#24471;&#21040;&#24403;&#21069;&#21442;&#25968;&#30340;&#21517;&#23383;&#21644;&#20540;
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node ID="ID_1493751203" CREATED="1541733194251" MODIFIED="1541733201519"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-indent: 0px; margin-top: 0; color: rgb(29, 31, 34); text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); text-transform: none; font-size: 14.4px; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; line-height: 1.5em; font-style: normal; white-space: normal" content="text/html; charset=utf-8" http-equiv="content-type">
      A search consists of:
    </p>
    <ul class="simple" style="text-indent: 0px; padding-top: 0px; margin-top: 0px; color: rgb(29, 31, 34); padding-left: 0px; text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); padding-right: 0px; text-transform: none; font-size: 14.4px; margin-bottom: 10px; padding-bottom: 0px; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 25px; font-style: normal; white-space: normal">
      <li style="line-height: 1.5em">
        an estimator (regressor or classifier such as<span>&#160;</span><span class="pre"><font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><code class="docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; background-color: rgb(236, 240, 243); padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">sklearn.svm.SVC()</code></font></span>);
      </li>
      <li style="line-height: 1.5em">
        a parameter space;
      </li>
      <li style="line-height: 1.5em">
        a method for searching or sampling candidates;
      </li>
      <li style="line-height: 1.5em">
        a cross-validation scheme; and
      </li>
      <li style="line-height: 1.5em">
        a<span>&#160;</span><span class="std std-ref"><font color="rgb(40, 120, 162)"><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" href="https://scikit-learn.org/stable/modules/grid_search.html#gridsearch-scoring">score function</a></font></span>.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node TEXT="GridSearchCV" FOLDED="true" ID="ID_1635477283" CREATED="1541733275618" MODIFIED="1541733451730"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25353;&#32473;&#23450;&#30340;&#21442;&#25968;&#65292;&#23436;&#25972;&#30340;&#25628;&#32034;&#25152;&#26377;&#30340;&#32452;&#21512;&#12290;&#20363;&#22914;&#19979;&#38754;&#30340;&#21442;&#25968;&#35774;&#32622;
    </p>
    <p>
      
    </p>
    <pre style="margin-right: 0px; text-transform: none; font-family: Monaco, Menlo, Consolas, Courier New, monospace; color: rgb(34, 34, 34); text-align: start; font-style: normal; margin-bottom: 0; padding-top: 5px; white-space: pre-wrap; padding-bottom: 5px; background-color: rgb(248, 248, 248); font-size: 13px; padding-right: 10px; padding-left: 10px; font-weight: 400; display: block; word-spacing: 0px; letter-spacing: normal; line-height: 1.2em; margin-top: 0; text-indent: 0px; margin-left: 0px" content="text/html; charset=utf-8" http-equiv="content-type"><span class="n">param_grid</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="p">[</span>
  <span class="p">{</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'C'</font></span><span class="p">:</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">10</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">100</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1000</font></span><span class="p">],</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'kernel'</font></span><span class="p">:</span> <span class="p">[</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'linear'</font></span><span class="p">]},</span>
  <span class="p">{</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'C'</font></span><span class="p">:</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">10</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">100</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1000</font></span><span class="p">],</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'gamma'</font></span><span class="p">:</span> <span class="p">[</span><span class="mf" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0.001</font></span><span class="p">,</span> <span class="mf" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0.0001</font></span><span class="p">],</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'kernel'</font></span><span class="p">:</span> <span class="p">[</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'rbf'</font></span><span class="p">]},</span>
 <span class="p">]</span></pre>
    <p>
      &#32473;&#20986;&#20102;&#20004;&#32452;&#65292;&#23545;&#20110;&#27599;&#19968;&#32452;&#20869;&#37096;&#65292;&#37117;&#23384;&#22312;&#33509;&#24178;&#32452;&#21512;&#65292;&#27599;&#19968;&#20010;&#32452;&#21512;&#37117;&#20250;&#34987;&#23581;&#35797;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_776846727" CREATED="1541733477489" MODIFIED="1541733483804"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p class="topic-title first" style="text-indent: 0px; margin-top: 0; color: rgb(29, 31, 34); text-align: start; font-weight: bold; text-transform: none; font-size: 1.1em; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; line-height: 1.5em; font-style: normal; white-space: normal" content="text/html; charset=utf-8" http-equiv="content-type">
      Examples:
    </p>
    <ul class="simple" style="text-indent: 0px; padding-top: 0px; margin-top: 0px; color: rgb(29, 31, 34); padding-left: 0px; text-align: start; font-weight: 400; padding-right: 0px; text-transform: none; font-size: 14.4px; margin-bottom: 10px; padding-bottom: 0px; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 25px; font-style: normal; white-space: normal">
      <li style="line-height: 1.5em">
        See<span>&#160;</span><span class="std std-ref"><font color="rgb(40, 120, 162)"><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" href="https://scikit-learn.org/stable/auto_examples/model_selection/plot_grid_search_digits.html#sphx-glr-auto-examples-model-selection-plot-grid-search-digits-py">Parameter estimation using grid search with cross-validation</a></font></span><span>&#160;</span>for an example of Grid Search computation on the digits dataset.
      </li>
      <li style="line-height: 1.5em">
        See<span>&#160;</span><span class="std std-ref"><font color="rgb(40, 120, 162)"><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" href="https://scikit-learn.org/stable/auto_examples/model_selection/grid_search_text_feature_extraction.html#sphx-glr-auto-examples-model-selection-grid-search-text-feature-extraction-py">Sample pipeline for text feature extraction and evaluation</a></font></span><span>&#160;</span>for an example of Grid Search coupling parameters from a text documents feature extractor (n-gram count vectorizer and TF-IDF transformer) with a classifier (here a linear SVM trained with SGD with either elastic net or L2 penalty) using a<span>&#160;</span><font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><span class="pre"><b><code class="xref py py-class docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; font-weight: bold; padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">pipeline.Pipeline</code></b></span></font><span>&#160;</span>instance.
      </li>
      <li style="line-height: 1.5em">
        See<span>&#160;</span><font color="rgb(40, 120, 162)"><span class="std std-ref"><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" href="https://scikit-learn.org/stable/auto_examples/model_selection/plot_nested_cross_validation_iris.html#sphx-glr-auto-examples-model-selection-plot-nested-cross-validation-iris-py">Nested versus non-nested cross-validation</a></span></font><span>&#160;</span>for an example of Grid Search within a cross validation loop on the iris dataset. This is the best practice for evaluating the performance of a model with grid search.
      </li>
      <li style="line-height: 1.5em">
        See<span>&#160;</span><font color="rgb(40, 120, 162)"><span class="std std-ref"><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" href="https://scikit-learn.org/stable/auto_examples/model_selection/plot_multi_metric_evaluation.html#sphx-glr-auto-examples-model-selection-plot-multi-metric-evaluation-py">Demonstration of multi-metric evaluation on cross_val_score and GridSearchCV</a></span></font><span>&#160;</span>for an example of<span>&#160;</span><font size="1.1em" face="monospace" color="black"><span class="pre"><code class="xref py py-class docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: black; padding-left: 4px; font-weight: bold; padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap"><b><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" title="sklearn.model_selection.GridSearchCV" href="https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV">GridSearchCV</a></b></code></span></font><span>&#160;</span>being used to evaluate multiple metrics simultaneously.
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_138385885" CREATED="1541946641988" MODIFIED="1541946844053"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      GridSearchCV&#30340;estimator&#20063;&#21487;&#20197;&#26159;pipeline&#65292;pipeline&#20197;&#23383;&#20856;&#30340;&#24418;&#24335;&#20256;&#20837;&#65292;&#23545;&#24212;&#30340;&#21442;&#25968;&#20063;&#26159;&#23383;&#20856;&#24418;&#24335;&#65292;key&#30340;&#32534;&#30721;&#26684;&#24335;&#26159;&lt;node_name&gt;__&lt;param_name&gt;&#65292;&#20854;&#20013;node_name&#23545;&#24212;pipeline&#23383;&#20856;&#20013;&#30340;&#26576;&#20010;key&#12290;&#20363;&#22914;
    </p>
    <p>
      
    </p>
    <p>
      <font face="Courier New">classifier = Pipeline([ </font>
    </p>
    <p>
      <font face="Courier New">&#160;&#160;&#160;&#160;('vectorizer', CountVectorizer(max_features=100000, ngram_range=(1, 3))), </font>
    </p>
    <p>
      <font face="Courier New">&#160;&#160;&#160;&#160;('clf', LogisticRegression(random_state=17))]) </font>
    </p>
    <p>
      <font face="Courier New">logit_pipe_params = {'clf__C': np.logspace(-1, 2, 4)} </font>
    </p>
    <p>
      <font face="Courier New">gcv = GridSearchCV(classifier, logit_pipe_params, n_jobs=-1, cv=skf, verbose=1, scoring='roc_auc')</font>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="RandomizedSearchCV" FOLDED="true" ID="ID_789674221" CREATED="1541733762920" MODIFIED="1541775241057"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25353;&#29031;&#19968;&#23450;&#30340;&#20998;&#24067;&#65292;&#23545;&#21442;&#25968;&#36827;&#34892;&#38543;&#26426;&#37319;&#26679;&#12290;&#25105;&#35273;&#24471;&#36825;&#31181;&#26041;&#27861;&#27604;&#36739;&#36866;&#21512;&#20154;&#24037;&#22823;&#33539;&#22260;&#23547;&#20248;&#65292;&#32780;GridSearchCV&#36866;&#21512;&#23567;&#33539;&#22260;&#35843;&#20248;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_1693492829" CREATED="1541733888695" MODIFIED="1541733969279"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-indent: 0px; margin-top: 0; color: rgb(29, 31, 34); text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); text-transform: none; font-size: 14.4px; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; line-height: 1.5em; font-style: normal; white-space: normal" content="text/html; charset=utf-8" http-equiv="content-type">
      &#20363;&#22914;&#19979;&#38754;&#36825;&#20010;SVM&#35774;&#32622;&#65292;&#23545;&#20110;C&#21644;gamma&#65292;&#25351;&#23450;&#20102;&#27010;&#29575;&#23494;&#24230;&#20989;&#25968;&#65292;&#23545;&#20110;class_weight&#65292;&#25351;&#23450;&#20102;&#21487;&#33021;&#30340;&#21462;&#20540;&#12290;
    </p>
    <p style="text-indent: 0px; margin-top: 0; color: rgb(29, 31, 34); text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); text-transform: none; font-size: 14.4px; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; line-height: 1.5em; font-style: normal; white-space: normal" content="text/html; charset=utf-8" http-equiv="content-type">
      
    </p>
    <p style="text-indent: 0px; margin-top: 0; color: rgb(29, 31, 34); text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); text-transform: none; font-size: 14.4px; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; line-height: 1.5em; font-style: normal; white-space: normal" content="text/html; charset=utf-8" http-equiv="content-type">
      For each parameter, either a distribution over possible values or a list of discrete choices (which will be sampled uniformly) can be specified:
    </p>
    <div class="highlight-default" style="text-indent: 0px; color: rgb(29, 31, 34); text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); text-transform: none; font-size: 14.4px; font-family: Helvetica, Arial, sans-serif; letter-spacing: normal; word-spacing: 0px; font-style: normal; white-space: normal">
      <div class="highlight" style="background-image: null; background-position: null; background-repeat: repeat; background-attachment: scroll">
        <pre style="padding-top: 5px; margin-top: 0; color: rgb(34, 34, 34); padding-left: 10px; display: block; background-color: rgb(248, 248, 248); padding-right: 10px; font-size: 13px; margin-bottom: 0; padding-bottom: 5px; font-family: Monaco, Menlo, Consolas, Courier New, monospace; margin-right: 0px; margin-left: 0px; line-height: 1.2em; white-space: pre-wrap">{<font color="rgb(64, 112, 160)">'C'</font>: scipy<font color="rgb(102, 102, 102)">.</font>stats<font color="rgb(102, 102, 102)">.</font>expon(scale<font color="rgb(102, 102, 102)">=</font><font color="rgb(32, 128, 80)">100</font>), <font color="rgb(64, 112, 160)">'gamma'</font>: scipy<font color="rgb(102, 102, 102)">.</font>stats<font color="rgb(102, 102, 102)">.</font>expon(scale<font color="rgb(102, 102, 102)">=.</font><font color="rgb(32, 128, 80)">1</font>),
  <font color="rgb(64, 112, 160)">'kernel'</font>: [<font color="rgb(64, 112, 160)">'rbf'</font>], <font color="rgb(64, 112, 160)">'class_weight'</font>:[<font color="rgb(64, 112, 160)">'balanced'</font>, <font color="rgb(0, 112, 32)"><b>None</b></font>]}</pre>
      </div>
    </div>
    <p style="text-indent: 0px; margin-top: 0; color: rgb(29, 31, 34); text-align: start; font-weight: 400; background-color: rgb(255, 255, 255); text-transform: none; font-size: 14.4px; margin-bottom: 0; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; line-height: 1.5em; font-style: normal; white-space: normal">
      This example uses the&#160;<font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><code class="docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; background-color: rgb(236, 240, 243); padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">scipy.stats</code></font>&#160;module, which contains many useful distributions for sampling parameters, such as&#160;<font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><code class="docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; background-color: rgb(236, 240, 243); padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">expon</code></font>,&#160;<font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><code class="docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; background-color: rgb(236, 240, 243); padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">gamma</code></font>,&#160;<font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><code class="docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; background-color: rgb(236, 240, 243); padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">uniform</code></font>&#160;or&#160;<font size="1.1em" face="monospace" color="rgb(34, 34, 34)"><code class="docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: rgb(34, 34, 34); padding-left: 4px; background-color: rgb(236, 240, 243); padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap">randint</code></font>.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_950226185" CREATED="1541733981959" MODIFIED="1541775241057"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <div class="topic" style="margin-right: 0px; text-transform: none; font-family: Helvetica, Arial, sans-serif; color: rgb(29, 31, 34); text-align: start; font-style: normal; margin-bottom: 10px; padding-top: 7px; white-space: normal; padding-bottom: 0px; background-color: rgb(238, 238, 238); font-size: 14.4px; padding-right: 7px; padding-left: 7px; font-weight: 400; word-spacing: 0px; letter-spacing: normal; margin-top: 10px; text-indent: 0px; margin-left: 0px" content="text/html; charset=utf-8" http-equiv="content-type">
      <p class="topic-title first" style="line-height: 1.5em; margin-right: 0px; margin-bottom: 0; margin-top: 0; font-size: 1.1em; font-weight: bold; margin-left: 0px">
        Examples:
      </p>
      <ul class="simple" style="margin-right: 0px; margin-bottom: 10px; padding-top: 0px; padding-left: 0px; margin-top: 0px; padding-bottom: 0px; margin-left: 25px; padding-right: 0px">
        <li style="line-height: 1.5em">
          <span class="std std-ref"><font color="rgb(40, 120, 162)"><a class="reference internal" style="color: rgb(40, 120, 162); text-decoration: none" href="https://scikit-learn.org/stable/auto_examples/model_selection/plot_randomized_search.html#sphx-glr-auto-examples-model-selection-plot-randomized-search-py">Comparing randomized search and grid search for hyperparameter estimation</a></font></span><span>&#160;</span>compares the usage and efficiency of randomized search and grid search.
        </li>
      </ul>
    </div>
    <div class="topic" style="text-indent: 0px; padding-top: 7px; margin-top: 10px; color: rgb(29, 31, 34); padding-left: 7px; text-align: start; font-weight: 400; background-color: rgb(238, 238, 238); padding-right: 7px; text-transform: none; font-size: 14.4px; margin-bottom: 10px; padding-bottom: 0px; font-family: Helvetica, Arial, sans-serif; margin-right: 0px; letter-spacing: normal; word-spacing: 0px; margin-left: 0px; font-style: normal; white-space: normal">
      <p class="topic-title first" style="line-height: 1.5em; margin-bottom: 0; margin-right: 0px; margin-top: 0; font-size: 1.1em; font-weight: bold; margin-left: 0px">
        References:
      </p>
      <ul class="simple" style="margin-right: 0px; margin-bottom: 10px; padding-top: 0px; padding-left: 0px; margin-top: 0px; padding-bottom: 0px; margin-left: 25px; padding-right: 0px">
        <li style="line-height: 1.5em">
          Bergstra, J. and Bengio, Y., Random search for hyper-parameter optimization, The Journal of Machine Learning Research (2012)
        </li>
      </ul>
    </div>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x53c2;&#x6570;&#x641c;&#x7d22;&#x7684;&#x6280;&#x5de7;" FOLDED="true" ID="ID_1155471663" CREATED="1541734048750" MODIFIED="1541775244927">
<node TEXT="&#x5b9a;&#x4e49;&#x5408;&#x9002;&#x7684;&#x5ea6;&#x91cf;&#x51fd;&#x6570;" ID="ID_838725774" CREATED="1541734059895" MODIFIED="1541734264867"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21487;&#20197;&#36890;&#36807;score&#26469;&#25351;&#23450;&#24230;&#37327;&#20989;&#25968;&#65292;&#19968;&#33324;&#26469;&#35828;&#65292;t<font face="Helvetica, Arial, sans-serif" color="rgb(29, 31, 34)">hese are the </font><font size="14.4px" face="Helvetica, Arial, sans-serif" color="rgb(40, 120, 162)"><code class="xref py py-func docutils literal" style="padding-top: 2px; border-left-width: medium; border-top-style: none; color: black; padding-left: 4px; font-weight: bold; padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap"><b><a class="reference internal" style="text-indent: 0px; color: rgb(40, 120, 162); text-decoration: none; text-align: start; background-color: rgb(255, 255, 255); font-weight: 400; text-transform: none; font-size: 14.4px; font-family: Helvetica, Arial, sans-serif; letter-spacing: normal; word-spacing: 0px; font-style: normal; white-space: normal" title="sklearn.metrics.accuracy_score" href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.accuracy_score.html#sklearn.metrics.accuracy_score"><u>sklearn.metrics.accuracy_score</u></a></b></code></font><font face="Helvetica, Arial, sans-serif" color="rgb(29, 31, 34)">&#160;for classification and<u>&#160;</u></font><u><font size="14.4px" face="Helvetica, Arial, sans-serif" color="rgb(40, 120, 162)"><code class="xref py py-func docutils literal" style="padding-top: 2px; border-top-style: none; border-left-width: medium; color: black; padding-left: 4px; font-weight: bold; padding-right: 4px; border-right-style: none; font-size: 1.1em; border-top-width: medium; padding-bottom: 2px; font-family: monospace; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; white-space: nowrap"><b><a class="reference internal" style="text-indent: 0px; color: rgb(40, 120, 162); text-decoration: none; text-align: start; background-color: rgb(255, 255, 255); font-weight: 400; text-transform: none; font-size: 14.4px; font-family: Helvetica, Arial, sans-serif; letter-spacing: normal; word-spacing: 0px; font-style: normal; white-space: normal" title="sklearn.metrics.r2_score" href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html#sklearn.metrics.r2_score">sklearn.metrics.r2_score</a></b></code></font></u><font face="Helvetica, Arial, sans-serif" color="rgb(29, 31, 34)">&#160;for regression</font>&#160;&#12290;<font face="Helvetica, Arial, sans-serif" color="rgb(29, 31, 34)">See&#160;</font><font size="14.4px" face="Helvetica, Arial, sans-serif" color="rgb(40, 120, 162)"><a class="reference internal" style="text-indent: 0px; color: rgb(40, 120, 162); text-decoration: none; text-align: start; background-color: rgb(255, 255, 255); font-weight: 400; text-transform: none; font-size: 14.4px; font-family: Helvetica, Arial, sans-serif; letter-spacing: normal; word-spacing: 0px; font-style: normal; white-space: normal" href="https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter">The scoring parameter: defining model evaluation rules</a></font><font face="Helvetica, Arial, sans-serif" color="rgb(29, 31, 34)">&#160;for more details.</font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x5b9a;&#x4e49;&#x591a;&#x4e2a;&#x5ea6;&#x91cf;&#x51fd;&#x6570;" ID="ID_853277443" CREATED="1541770898109" MODIFIED="1541771068067"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span charset="utf-8" style="color: rgb(29, 31, 34); font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none"><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px">See</font></span><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px"><span>&#160;</span></font><a class="reference internal" href="https://scikit-learn.org/stable/modules/model_evaluation.html#multimetric-scoring" style="color: rgb(40, 120, 162); text-decoration: none; font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)"><font color="rgb(40, 120, 162)" face="Helvetica, Arial, sans-serif" size="14.4px"><span class="std std-ref">Using multiple metric evaluation</span></font></a><span><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px">&#160;</font></span><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px"><span style="color: rgb(29, 31, 34); font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none">for more details. </span></font>
    </p>
    <p>
      
    </p>
    <p>
      refit&#38656;&#35201;&#35774;&#32622;&#65292;&#35774;&#32622;&#20026;&#24230;&#37327;&#30340;&#21517;&#23383;(&#23383;&#31526;&#20018;)&#65292;&#20197;&#20415;best_params_&#21644;best_estimator_&#21487;&#20197;&#20351;&#29992;&#12290;&#22914;&#26524;&#19981;&#24819;&#35201;&#65292;&#21487;&#20197;&#35774;&#32622;refit&#20026;False&#12290;
    </p>
    <p>
      
    </p>
    <p>
      <a charset="utf-8" class="reference internal" href="https://scikit-learn.org/stable/auto_examples/model_selection/plot_multi_metric_evaluation.html#sphx-glr-auto-examples-model-selection-plot-multi-metric-evaluation-py" style="color: rgb(5, 87, 129); text-decoration: underline; font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)"><font color="rgb(5, 87, 129)" face="Helvetica, Arial, sans-serif" size="14.4px"><u><span class="std std-ref">Demonstration of multi-metric evaluation on cross_val_score and GridSearchCV</span></u></font></a><span><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px">&#160;</font></span><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px"><span style="color: rgb(29, 31, 34); font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none">for an example usage.</span></font>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x7ec4;&#x5408;&#x591a;&#x4e2a;estimator" ID="ID_731231040" CREATED="1541771101374" MODIFIED="1541775244926"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <a charset="utf-8" class="reference internal" href="https://scikit-learn.org/stable/modules/compose.html#pipeline" style="color: rgb(40, 120, 162); text-decoration: none; font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)"><font color="rgb(40, 120, 162)" face="Helvetica, Arial, sans-serif" size="14.4px"><span class="std std-ref">Pipeline: chaining estimators</span></font></a><span><font color="rgb(29, 31, 34)" face="Helvetica, Arial, sans-serif" size="14.4px">&#160; </font></span>
    </p>
    <p>
      
    </p>
    <p>
      &#36890;&#36807;&#31649;&#36947;&#21487;&#20197;&#23558;&#22810;&#20010;estimator&#36830;&#25509;&#36215;&#26469;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x533a;&#x5206;development set&#x548c;evaluation set" ID="ID_1132783222" CREATED="1541771551101" MODIFIED="1541771678521"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#27169;&#22411;&#36873;&#25321;&#26412;&#36523;&#20063;&#21487;&#20197;&#30475;&#20316;&#26159;&#22312;&#20351;&#29992;&#25968;&#25454;&#22312;&#35757;&#32451;&#21442;&#25968;&#65292;&#25152;&#20197;&#26377;&#24517;&#35201;&#25343;&#20986;&#19968;&#37096;&#20998;&#25968;&#25454;&#26469;&#24230;&#37327;&#27169;&#22411;&#65292;&#25152;&#20197;&#38656;&#35201;&#20351;&#29992;train_test_split&#26469;&#21306;&#20998;development set&#21644;evaluation set.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x5e76;&#x884c;" ID="ID_125260729" CREATED="1541771691860" MODIFIED="1541771930938"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      GridSearchCV&#21644;RandomizedSearchCV&#30340;&#21442;&#25968;&#25628;&#32034;&#37117;&#26159;&#24444;&#27492;&#29420;&#31435;&#30340;&#65292;&#25152;&#20197;&#21487;&#20197;&#36890;&#36807;n_jobs&#26469;&#25351;&#23450;&#21516;&#26102;&#36816;&#34892;&#22810;&#20010;&#20219;&#21153;&#12290;n_jobs&#35774;&#25104;-1&#65292;&#23601;&#26159;&#20351;&#29992;&#25152;&#26377;&#21487;&#29992;&#30340;&#26680;; &#35774;&#25104;None&#65292;&#21017;&#26159;&#21482;&#20351;&#29992;1&#20010;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="&#x5bf9;&#x5931;&#x8d25;&#x9c81;&#x68d2;" ID="ID_1660695472" CREATED="1541771701260" MODIFIED="1541772025649"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#26377;&#26102;&#65292;&#25628;&#32034;&#36807;&#31243;&#65292;&#23545;&#26576;&#20123;&#21442;&#25968;&#21487;&#33021;&#20250;&#22833;&#36133;&#12290;&#23558;&#21442;&#25968;error_scores&#35774;&#25104;0&#65292;&#21487;&#20197;&#36991;&#20813;&#22240;&#27492;&#25972;&#20010;&#25628;&#32034;&#36807;&#31243;&#37117;&#34987;&#21462;&#28040;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="&#x7279;&#x5b9a;&#x7684;CV" ID="ID_1973648926" CREATED="1541774230791" MODIFIED="1541774280860" LINK="https://scikit-learn.org/stable/modules/grid_search.html#model-specific-cross-validation"/>
</node>
<node TEXT="Model Evaluation" LOCALIZED_STYLE_REF="styles.subtopic" FOLDED="true" ID="ID_58502383" CREATED="1541775283901" MODIFIED="1541832590149" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html">
<node TEXT="scoring strategy" FOLDED="true" ID="ID_1127053148" CREATED="1541775423995" MODIFIED="1541803394862"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21069;&#38754;&#30340;cross_val_score&#65292;GridSearchCV&#31561;&#22312;&#35780;&#20272;estimator&#30340;&#26102;&#20505;&#65292;&#26681;&#25454;&#21442;&#25968;scoring&#26469;&#30830;&#23450;&#22914;&#20309;&#35745;&#31639;&#22909;&#22351;&#12290;&#36825;&#37324;&#26377;&#19968;&#20010;&#32422;&#23450;&#65292;scoring&#30340;&#20540;&#36234;&#22823;&#36234;&#22909;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x9884;&#x5b9a;&#x4e49;&#x597d;&#x7684;scoring" ID="ID_1635513340" CREATED="1541775550675" MODIFIED="1541775579539" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#common-cases-predefined-values"/>
<node TEXT="use metric functions to define scoring strategy" FOLDED="true" ID="ID_216484873" CREATED="1541803397234" MODIFIED="1541803678916"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#24456;&#22810;&#24230;&#37327;&#26041;&#27861;(metric function)&#24182;&#27809;&#26377;&#32473;&#20986;&#23383;&#31526;&#20018;&#21517;&#23383;&#65292;&#21487;&#20197;&#30452;&#25509;&#20256;&#32473;scoring&#21442;&#25968;&#65292;&#37027;&#26102;&#22240;&#20026;&#36825;&#20123;&#24230;&#37327;&#36824;&#38656;&#35201;&#39069;&#22806;&#30340;&#21442;&#25968;&#65292;&#36825;&#26102;&#20505;&#65292;&#38656;&#35201;make_scoer&#26469;&#26500;&#36896;&#24230;&#37327;&#23376;&#65292;&#20877;&#20256;&#32473;scoring&#21442;&#25968;&#12290;scoring&#21442;&#25968;&#21487;&#20197;&#25509;&#21463;&#23383;&#31526;&#20018;&#65292;&#20063;&#21487;&#20197;&#25509;&#21463;&#24230;&#37327;&#23376;&#65292;&#36824;&#21487;&#20197;&#25509;&#21463;&#20182;&#20204;&#30340;&#21015;&#34920;&#12290;
    </p>
    <p>
      
    </p>
    <ul charset="utf-8" class="simple" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 25px; color: rgb(29, 31, 34); font-family: Helvetica, Arial, sans-serif; font-size: 14.4px; font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; background-color: rgb(255, 255, 255)">
      <li style="line-height: 1.5em">
        functions ending with<span>&#160;</span><code class="docutils literal" style="padding-top: 2px; padding-bottom: 2px; padding-right: 4px; padding-left: 4px; font-family: monospace; font-size: 1.1em; color: rgb(34, 34, 34); background-color: rgb(236, 240, 243); border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; white-space: nowrap"><font face="monospace" size="1.1em" color="rgb(34, 34, 34)"><span class="pre">_score</span></font></code><span>&#160;</span>return a value to maximize, the higher the better.
      </li>
      <li style="line-height: 1.5em">
        functions ending with<span>&#160;</span><code class="docutils literal" style="padding-top: 2px; padding-bottom: 2px; padding-right: 4px; padding-left: 4px; font-family: monospace; font-size: 1.1em; color: rgb(34, 34, 34); background-color: rgb(236, 240, 243); border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; white-space: nowrap"><font face="monospace" size="1.1em" color="rgb(34, 34, 34)"><span class="pre">_error</span></font></code><span>&#160;</span>or<span>&#160;</span><code class="docutils literal" style="padding-top: 2px; padding-bottom: 2px; padding-right: 4px; padding-left: 4px; font-family: monospace; font-size: 1.1em; color: rgb(34, 34, 34); background-color: rgb(236, 240, 243); border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; white-space: nowrap"><font face="monospace" size="1.1em" color="rgb(34, 34, 34)"><span class="pre">_loss</span></font></code><span>&#160;</span>return a value to minimize, the lower the better. When converting into a scorer object using<span>&#160;</span><a class="reference internal" href="https://scikit-learn.org/stable/modules/generated/sklearn.metrics.make_scorer.html#sklearn.metrics.make_scorer" title="sklearn.metrics.make_scorer" style="color: rgb(40, 120, 162); text-decoration: none"><font color="black" face="monospace" size="1.1em"><code class="xref py py-func docutils literal" style="padding-top: 2px; padding-bottom: 2px; padding-right: 4px; padding-left: 4px; font-family: monospace; font-size: 1.1em; color: black; border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; white-space: nowrap; font-weight: bold"><b><span class="pre">make_scorer</span></b></code></font></a>, set the<span>&#160;</span><code class="docutils literal" style="padding-top: 2px; padding-bottom: 2px; padding-right: 4px; padding-left: 4px; font-family: monospace; font-size: 1.1em; color: rgb(34, 34, 34); background-color: rgb(236, 240, 243); border-top-style: none; border-top-width: medium; border-right-style: none; border-right-width: medium; border-bottom-style: none; border-bottom-width: medium; border-left-style: none; border-left-width: medium; white-space: nowrap"><font face="monospace" size="1.1em" color="rgb(34, 34, 34)"><span class="pre">greater_is_better</span></font></code><span>&#160;</span>parameter to False (True by default; see the parameter description below).
      </li>
    </ul>
  </body>
</html>
</richcontent>
<node ID="ID_697066331" CREATED="1541803568845" MODIFIED="1541803587701"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre charset="utf-8" style="padding-top: 5px; padding-bottom: 5px; padding-right: 10px; padding-left: 10px; font-family: Monaco, Menlo, Consolas, Courier New, monospace; font-size: 13px; color: rgb(34, 34, 34); display: block; margin-top: 0; margin-right: 0px; margin-bottom: 0; margin-left: 0px; line-height: 1.2em; white-space: pre-wrap; background-color: rgb(248, 248, 248); font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; word-spacing: 0px"><span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.metrics</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">fbeta_score</span><span class="p">,</span> <span class="n">make_scorer</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">ftwo_scorer</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">fbeta_score</span><span class="p">,</span> <span class="n">beta</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">2</font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.model_selection</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">GridSearchCV</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.svm</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">LinearSVC</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">grid</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">GridSearchCV</span><span class="p">(</span><span class="n">LinearSVC</span><span class="p">(),</span> <span class="n">param_grid</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="p">{</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'C'</font></span><span class="p">:</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">10</font></span><span class="p">]},</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>                    <span class="n">scoring</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="n">ftwo_scorer</span><span class="p">,</span> <span class="n">cv</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">5</font></span><span class="p">)</span></pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node ID="ID_1560963931" CREATED="1541805237088" MODIFIED="1541805451471"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      &#33258;&#23450;&#20041;&#24230;&#37327;&#20989;&#25968;
    </p>
    <p style="text-align: left">
      &#24230;&#37327;&#20989;&#25968;&#30340;&#24418;&#24335;&#26159;f(X, y)&#12290;&#20256;&#32473;make_scorer&#21019;&#24314;&#24230;&#37327;&#23376;&#30340;&#26102;&#20505;&#65292;&#38656;&#35201;&#25351;&#23450;greater_is_better,&#22914;&#26524;&#26159;&#20998;&#31867;&#24230;&#37327;&#65292;&#38656;&#35201;&#25351;&#23450;needs_threshold&#12290;
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <pre charset="utf-8" style="padding-top: 5px; padding-bottom: 5px; padding-right: 10px; padding-left: 10px; font-family: Monaco, Menlo, Consolas, Courier New, monospace; font-size: 13px; color: rgb(34, 34, 34); display: block; margin-top: 0; margin-right: 0px; margin-bottom: 0; margin-left: 0px; line-height: 1.2em; white-space: pre-wrap; background-color: rgb(248, 248, 248); font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; word-spacing: 0px"><span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">import</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>numpy</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>as</b></font></span> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>np</b></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">def</font></span></b> <span class="nf" style="color: rgb(6, 40, 126)"><font color="rgb(6, 40, 126)">my_custom_loss_func</font></span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">):</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span class="n">diff</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">np</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">abs</span><span class="p">(</span><span class="n">y_true</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">-</font></span> <span class="n">y_pred</span><span class="p">)</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">max</span><span class="p">()</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>    <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>return</b></font></span> <span class="n">np</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">log1p</span><span class="p">(</span><span class="n">diff</span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>...</b></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="c1" style="color: rgb(64, 128, 144); font-style: italic"><font color="rgb(64, 128, 144)"><i># score will negate the return value of my_custom_loss_func,</i></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="c1" style="color: rgb(64, 128, 144); font-style: italic"><font color="rgb(64, 128, 144)"><i># which will be np.log(2), 0.693, given the values for X</i></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="c1" style="color: rgb(64, 128, 144); font-style: italic"><font color="rgb(64, 128, 144)"><i># and y defined below.</i></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">score</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">my_custom_loss_func</span><span class="p">,</span> <span class="n">greater_is_better</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="kc" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>False</b></font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">X</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="p">[[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">],</span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">]]</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">y</span>  <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="p">[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">]</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.dummy</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">DummyClassifier</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">clf</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">DummyClassifier</span><span class="p">(</span><span class="n">strategy</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'most_frequent'</font></span><span class="p">,</span> <span class="n">random_state</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">clf</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">clf</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">fit</span><span class="p">(</span><span class="n">X</span><span class="p">,</span> <span class="n">y</span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">my_custom_loss_func</span><span class="p">(</span><span class="n">clf</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">predict</span><span class="p">(</span><span class="n">X</span><span class="p">),</span> <span class="n">y</span><span class="p">)</span> 
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">0.69...</font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">score</span><span class="p">(</span><span class="n">clf</span><span class="p">,</span> <span class="n">X</span><span class="p">,</span> <span class="n">y</span><span class="p">)</span> 
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">-0.69...</font></span></pre>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_822742778" CREATED="1541805490146" MODIFIED="1541805594666"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      &#33258;&#23450;&#20041;&#24230;&#37327;&#23376;
    </p>
    <p style="text-align: left">
      &#28385;&#36275;&#22914;&#19979;&#26465;&#20214;&#21363;&#21487;
    </p>
    <ul>
      <li>
        &#33021;&#22815;&#34987;&#35843;&#29992;&#65292;&#24418;&#24335;&#26159;f(estimator, X, y)
      </li>
      <li>
        &#36820;&#22238;&#28014;&#28857;&#25968;&#12290;&#23545;&#20110;loss&#65292;&#20445;&#35777;&#36234;&#23567;&#36234;&#22909;&#65292;&#23545;&#20110;score&#65292;&#20445;&#35777;&#36234;&#22823;&#36234;&#22909;
      </li>
    </ul>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_514168523" CREATED="1541805614010" MODIFIED="1541805786315"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      &#20351;&#29992;&#22810;&#20010;&#24230;&#37327;
    </p>
    <p style="text-align: left">
      &#20004;&#31181;&#26041;&#27861;&#65292;&#19968;&#31181;&#26159;&#23383;&#31526;&#20018;&#21015;&#34920;&#65292;
    </p>
    <pre charset="utf-8" style="padding-top: 5px; padding-bottom: 5px; padding-right: 10px; padding-left: 10px; font-family: Monaco, Menlo, Consolas, Courier New, monospace; font-size: 13px; color: rgb(34, 34, 34); display: block; margin-top: 0; margin-right: 0px; margin-bottom: 0; margin-left: 0px; line-height: 1.2em; white-space: pre-wrap; background-color: rgb(248, 248, 248); font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; word-spacing: 0px"><span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">scoring</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="p">[</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'accuracy'</font></span><span class="p">,</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'precision'</font></span><span class="p">]</span></pre>
    <p style="text-align: left">
      &#19968;&#31181;&#26159;&#23383;&#20856;&#65292;
    </p>
    <pre charset="utf-8" style="padding-top: 5px; padding-bottom: 5px; padding-right: 10px; padding-left: 10px; font-family: Monaco, Menlo, Consolas, Courier New, monospace; font-size: 13px; color: rgb(34, 34, 34); display: block; margin-top: 0; margin-right: 0px; margin-bottom: 0; margin-left: 0px; line-height: 1.2em; white-space: pre-wrap; background-color: rgb(248, 248, 248); font-style: normal; font-weight: 400; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; word-spacing: 0px"><span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.metrics</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">accuracy_score</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.metrics</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">make_scorer</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">scoring</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="p">{</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'accuracy'</font></span><span class="p">:</span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">accuracy_score</span><span class="p">),</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>           <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'prec'</font></span><span class="p">:</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'precision'</font></span><span class="p">}</span></pre>
  </body>
</html>
</richcontent>
<node ID="ID_565062554" CREATED="1541805789338" MODIFIED="1541805797704"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre charset="utf-8" style="padding-top: 5px; padding-bottom: 5px; padding-right: 10px; padding-left: 10px; font-family: Monaco, Menlo, Consolas, Courier New, monospace; font-size: 13px; color: rgb(34, 34, 34); display: block; margin-top: 0; margin-right: 0px; margin-bottom: 0; margin-left: 0px; line-height: 1.2em; white-space: pre-wrap; background-color: rgb(248, 248, 248); font-style: normal; font-weight: 400; letter-spacing: normal; text-align: start; text-indent: 0px; text-transform: none; word-spacing: 0px"><span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.model_selection</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">cross_validate</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="kn" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">from</font></span></b> <span class="nn" style="color: rgb(14, 132, 181); font-weight: bold"><font color="rgb(14, 132, 181)"><b>sklearn.metrics</b></font></span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>import</b></font></span> <span class="n">confusion_matrix</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="c1" style="color: rgb(64, 128, 144); font-style: italic"><font color="rgb(64, 128, 144)"><i># A sample toy binary classification dataset</i></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">X</span><span class="p">,</span> <span class="n">y</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">datasets</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">make_classification</span><span class="p">(</span><span class="n">n_classes</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">2</font></span><span class="p">,</span> <span class="n">random_state</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">svm</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">LinearSVC</span><span class="p">(</span><span class="n">random_state</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">def</font></span></b> <span class="nf" style="color: rgb(6, 40, 126)"><font color="rgb(6, 40, 126)">tn</font></span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">):</span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>return</b></font></span> <span class="n">confusion_matrix</span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">)[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">]</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">def</font></span></b> <span class="nf" style="color: rgb(6, 40, 126)"><font color="rgb(6, 40, 126)">fp</font></span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">):</span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>return</b></font></span> <span class="n">confusion_matrix</span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">)[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">]</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">def</font></span></b> <span class="nf" style="color: rgb(6, 40, 126)"><font color="rgb(6, 40, 126)">fn</font></span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">):</span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>return</b></font></span> <span class="n">confusion_matrix</span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">)[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">0</font></span><span class="p">]</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><b><span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)">def</font></span></b> <span class="nf" style="color: rgb(6, 40, 126)"><font color="rgb(6, 40, 126)">tp</font></span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">):</span> <span class="k" style="color: rgb(0, 112, 32); font-weight: bold"><font color="rgb(0, 112, 32)"><b>return</b></font></span> <span class="n">confusion_matrix</span><span class="p">(</span><span class="n">y_true</span><span class="p">,</span> <span class="n">y_pred</span><span class="p">)[</span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">,</span> <span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">1</font></span><span class="p">]</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">scoring</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="p">{</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'tp'</font></span> <span class="p">:</span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">tp</span><span class="p">),</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'tn'</font></span> <span class="p">:</span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">tn</span><span class="p">),</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>           <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'fp'</font></span> <span class="p">:</span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">fp</span><span class="p">),</span> <span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'fn'</font></span> <span class="p">:</span> <span class="n">make_scorer</span><span class="p">(</span><span class="n">fn</span><span class="p">)}</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="n">cv_results</span> <span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span> <span class="n">cross_validate</span><span class="p">(</span><span class="n">svm</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">.</font></span><span class="n">fit</span><span class="p">(</span><span class="n">X</span><span class="p">,</span> <span class="n">y</span><span class="p">),</span> <span class="n">X</span><span class="p">,</span> <span class="n">y</span><span class="p">,</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>... </b></font></span>                            <span class="n">scoring</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="n">scoring</span><span class="p">,</span> <span class="n">cv</span><span class="o" style="color: rgb(102, 102, 102)"><font color="rgb(102, 102, 102)">=</font></span><span class="mi" style="color: rgb(32, 128, 80)"><font color="rgb(32, 128, 80)">5</font></span><span class="p">)</span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="c1" style="color: rgb(64, 128, 144); font-style: italic"><font color="rgb(64, 128, 144)"><i># Getting the test set true positive scores</i></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="nb" style="color: rgb(0, 112, 32)"><font color="rgb(0, 112, 32)">print</font></span><span class="p">(</span><span class="n">cv_results</span><span class="p">[</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'test_tp'</font></span><span class="p">])</span>  
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">[10  9  8  7  8]</font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="c1" style="color: rgb(64, 128, 144); font-style: italic"><font color="rgb(64, 128, 144)"><i># Getting the test set false negative scores</i></font></span>
<span class="gp" style="color: rgb(198, 93, 9); font-weight: bold"><font color="rgb(198, 93, 9)"><b>&gt;&gt;&gt; </b></font></span><span class="nb" style="color: rgb(0, 112, 32)"><font color="rgb(0, 112, 32)">print</font></span><span class="p">(</span><span class="n">cv_results</span><span class="p">[</span><span class="s1" style="color: rgb(64, 112, 160)"><font color="rgb(64, 112, 160)">'test_fn'</font></span><span class="p">])</span>  
<span class="go" style="color: rgb(51, 51, 51)"><font color="rgb(51, 51, 51)">[0 1 2 3 2]</font></span></pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Classification metrics" ID="ID_1997205616" CREATED="1541803372010" MODIFIED="1541831911278" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#classification-metrics"/>
<node ID="ID_1878117693" CREATED="1541806092232" MODIFIED="1541831928195" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#multilabel-ranking-metrics"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      Multilabel randing metrics
    </p>
    <p style="text-align: left">
      &#21516;&#19968;&#20010;&#25968;&#25454;&#20855;&#26377;&#22810;&#20010;&#26631;&#31614;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Regression metrics" ID="ID_1131428490" CREATED="1541806058992" MODIFIED="1541831942038" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#regression-metrics"/>
<node TEXT="Cluster metrics" ID="ID_216622902" CREATED="1541806146464" MODIFIED="1541831959441" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#clustering-metrics"/>
<node TEXT="Dummy Estimator" FOLDED="true" ID="ID_1764479266" CREATED="1541807064743" MODIFIED="1541807534796" LINK="https://scikit-learn.org/stable/modules/model_evaluation.html#dummy-estimators"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#24403;&#36827;&#34892;&#26377;&#30417;&#30563;&#23398;&#20064;&#30340;&#26102;&#20505;&#65292;&#23558;&#23398;&#20064;&#22120;&#21644;&#22522;&#20110;&#31616;&#21333;&#30340;&#35268;&#21017;&#30340;&#21028;&#26029;&#20570;&#20010;&#27604;&#36739;&#20250;&#26159;&#19968;&#20010;&#19981;&#38169;&#30340;&#26816;&#26597;&#65292;&#21487;&#20197;&#36991;&#20813;&#26126;&#26174;&#30340;&#38169;&#35823;&#12290;&#25152;&#35859;&#31616;&#21333;&#30340;&#35268;&#21017;&#65292;&#20363;&#22914;&#65292;&#26681;&#25454;&#35757;&#32451;&#38598;&#30340;&#20998;&#24067;&#65292;&#24635;&#26159;&#36820;&#22238;&#26576;&#20010;&#25968;&#37327;&#26368;&#22810;&#30340;&#31867;&#21035;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="" ID="ID_1906555674" CREATED="1541807490632" MODIFIED="1541807510604">
<hook URI="../../xytnotebook/myMindMap/pics/dummyclassifier.jpg" SIZE="0.456621" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="Model persistence" FOLDED="true" ID="ID_319768596" CREATED="1541832171776" MODIFIED="1541832566811" LINK="https://scikit-learn.org/stable/modules/model_persistence.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20445;&#23384;&#23398;&#20064;&#22909;&#30340;&#27169;&#22411;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="pickle" FOLDED="true" ID="ID_1300479651" CREATED="1541832182337" MODIFIED="1541832185988">
<node ID="ID_1572203394" CREATED="1541832204992" MODIFIED="1541832209493"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre style="text-transform: none; line-height: 1.2em; text-align: start; padding-top: 5px; margin-left: 0px; padding-right: 10px; background-color: rgb(248, 248, 248); white-space: pre-wrap; color: rgb(34, 34, 34); text-indent: 0px; padding-bottom: 5px; font-weight: 400; letter-spacing: normal; margin-bottom: 0; font-style: normal; font-family: Monaco, Menlo, Consolas, Courier New, monospace; margin-right: 0px; padding-left: 10px; word-spacing: 0px; font-size: 13px; display: block; margin-top: 0" http-equiv="content-type" content="text/html; charset=utf-8"><b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="kn">import</span></font></b> <b><font color="rgb(14, 132, 181)"><span style="color: rgb(14, 132, 181); font-weight: bold" class="nn">pickle</span></font></b>
<b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font></b><span class="n">s</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">pickle</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">dumps</span><span class="p">(</span><span class="n">clf</span><span class="p">)</span>
<b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font></b><span class="n">clf2</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">pickle</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">loads</span><span class="p">(</span><span class="n">s</span><span class="p">)</span>
<b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font></b><span class="n">clf2</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">predict</span><span class="p">(</span><span class="n">X</span><span class="p">[</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">0</span></font><span class="p">:</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">1</span></font><span class="p">])</span>
<font color="rgb(51, 51, 51)"><span style="color: rgb(51, 51, 51)" class="go">array([0])</span></font>
<b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font></b><span class="n">y</span><span class="p">[</span><font color="rgb(32, 128, 80)"><span style="color: rgb(32, 128, 80)" class="mi">0</span></font><span class="p">]</span>
<font color="rgb(51, 51, 51)"><span style="color: rgb(51, 51, 51)" class="go">0</span></font></pre>
  </body>
</html>
</richcontent>
</node>
</node>
<node FOLDED="true" ID="ID_444932712" CREATED="1541832190713" MODIFIED="1541833012047"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: right">
      joblib
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20248;&#28857;&#26159;&#36866;&#21512;&#22823;&#22411;numpy&#25968;&#32452;&#12290;&#38480;&#21046;&#26159;&#21482;&#33021;dump&#21040;&#30828;&#30424;&#65292;&#19981;&#33021;dump&#21040;&#23383;&#33410;&#27969;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_902718287" CREATED="1541832229536" MODIFIED="1541832240719"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <pre style="text-transform: none; line-height: 1.2em; text-align: start; padding-top: 5px; margin-left: 0px; padding-right: 10px; background-color: rgb(248, 248, 248); white-space: pre-wrap; color: rgb(34, 34, 34); text-indent: 0px; padding-bottom: 5px; font-weight: 400; letter-spacing: normal; margin-bottom: 0; font-style: normal; font-family: Monaco, Menlo, Consolas, Courier New, monospace; margin-right: 0px; padding-left: 10px; word-spacing: 0px; font-size: 13px; display: block; margin-top: 0" http-equiv="content-type" content="text/html; charset=utf-8"><b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="kn">from</span></font></b> <b><font color="rgb(14, 132, 181)"><span style="color: rgb(14, 132, 181); font-weight: bold" class="nn">sklearn.externals</span></font></b> <b><font color="rgb(0, 112, 32)"><span style="color: rgb(0, 112, 32); font-weight: bold" class="k">import</span></font></b> <span class="n">joblib</span>
<b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font></b><span class="n">joblib</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">dump</span><span class="p">(</span><span class="n">clf</span><span class="p">,</span> <font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s1">'filename.joblib'</span></font><span class="p">)</span> </pre>
    <pre style="text-transform: none; line-height: 1.2em; text-align: start; padding-top: 5px; margin-left: 0px; padding-right: 10px; background-color: rgb(248, 248, 248); white-space: pre-wrap; color: rgb(34, 34, 34); text-indent: 0px; padding-bottom: 5px; font-weight: 400; letter-spacing: normal; margin-bottom: 0; font-style: normal; font-family: Monaco, Menlo, Consolas, Courier New, monospace; margin-right: 0px; padding-left: 10px; word-spacing: 0px; font-size: 13px; display: block; margin-top: 0" http-equiv="content-type" content="text/html; charset=utf-8"><b><font color="rgb(198, 93, 9)"><span style="color: rgb(198, 93, 9); font-weight: bold" class="gp">&gt;&gt;&gt; </span></font></b><span class="n">clf</span> <font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">=</span></font> <span class="n">joblib</span><font color="rgb(102, 102, 102)"><span style="color: rgb(102, 102, 102)" class="o">.</span></font><span class="n">load</span><span class="p">(</span><font color="rgb(64, 112, 160)"><span style="color: rgb(64, 112, 160)" class="s1">'filename.joblib'</span></font><span class="p">)</span> </pre>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Validation curve" FOLDED="true" ID="ID_189580044" CREATED="1541832354264" MODIFIED="1541832554107" LINK="https://scikit-learn.org/stable/modules/learning_curve.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#32472;&#21046;&#23398;&#20064;&#26354;&#32447;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="&#x5bf9;&#x6570;&#x5750;&#x6807;" FOLDED="true" ID="ID_245276866" CREATED="1541834714075" MODIFIED="1541835210791"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#25628;&#32034;&#21442;&#25968;&#31354;&#38388;&#30340;&#26102;&#20505;&#65292;&#32463;&#24120;&#20250;&#20351;&#29992;np.logspace&#65292;&#20197;&#26041;&#20415;&#22312;&#23545;&#25968;&#22352;&#26631;&#19978;&#24605;&#32771;&#21442;&#25968;&#31354;&#38388;&#12290;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="np.logspace" ID="ID_505192444" CREATED="1541834802194" MODIFIED="1541835077515"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      np.logspace(-6, 2, 9)&#34920;&#31034;&#29983;&#25104;9&#20010;&#23545;&#25968;&#22352;&#26631;&#65292;[-6, 2]&#20043;&#38388;&#30340;&#25972;&#25968;&#12290;&#25152;&#35859;<b>&#23545;&#25968;&#22352;&#26631;</b>&#65292;&#19981;&#22952;&#35774;&#20026;x'&#65292;&#21407;&#22987;&#22352;&#26631;&#35774;&#20026;x&#65292;&#21017;x' = log(x)&#12290;&#36825;&#37324;&#30340;[-6, 2]&#20043;&#38388;&#30340;9&#20010;&#25972;&#25968;&#23601;&#26159;&#23601;&#23545;&#25968;&#22352;&#26631;&#32780;&#35328;&#12290;np.logspace&#36820;&#22238;&#30340;&#26159;&#23545;&#24212;&#30340;&#21407;&#22987;&#22352;&#26631;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="plt.semilogx, plt.semilogy" ID="ID_28936390" CREATED="1541834946554" MODIFIED="1541835620430"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21644;plt.plot&#30340;&#24046;&#21035;&#26159;&#23558;&#20256;&#20837;&#30340;x/y&#22352;&#26631;&#32472;&#21046;&#22312;&#23545;&#25968;&#22352;&#26631;&#19978;&#12290;&#27880;&#24847;&#36825;&#37324;&#20256;&#20837;&#30340;&#22352;&#26631;&#36824;&#26159;&#22312;&#21407;&#22987;&#22352;&#26631;&#19978;&#12290;&#22240;&#27492;&#65292;plt.semilogx, plt.semilogy&#24120;&#24120;&#21644;np.logspace&#19968;&#36215;&#20351;&#29992;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="Plot Validation Curve" ID="ID_1122364928" CREATED="1541836536574" MODIFIED="1541836776981" LINK="https://scikit-learn.org/stable/auto_examples/model_selection/plot_validation_curve.html#sphx-glr-auto-examples-model-selection-plot-validation-curve-py"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      validation_curve
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Plot Learning Curve" ID="ID_581539638" CREATED="1541836713966" MODIFIED="1541836759887" LINK="https://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html#sphx-glr-auto-examples-model-selection-plot-learning-curve-py"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      learning_curve
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Data preprocess and Pipeline" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_658816784" CREATED="1541601386482" MODIFIED="1541837034580">
<node TEXT="&#x6570;&#x636e;&#x9884;&#x5904;&#x7406;&#x7b49;&#x8f6c;&#x6362;&#x5e94;&#x7528;&#x5230;&#x8bad;&#x7ec3;&#x96c6;&#xff0c;&#x540c;&#x65f6;&#x4e5f;&#x9700;&#x8981;&#x5e94;&#x7528;&#x5230;&#x6d4b;&#x8bd5;&#x96c6;&#xff0c;&#x5728;sklearn&#x4e2d;&#x6709;&#x4e24;&#x79cd;&#x65b9;&#x6cd5;&#xff0c;&#x4e00;&#x79cd;&#x76f4;&#x89c2;&#xff0c;&#x4e00;&#x79cd;&#x65b9;&#x4fbf;:&#xa;&#xa;from sklearn import preprocessing&#xa;X_train, X_test, y_train, y_test = train_test_split(&#xa;    iris.data, iris.target, test_size=0.4, random_state=0)&#xa;scaler = preprocessing.StandardScaler().fit(X_train)&#xa;X_train_transformed = scaler.transform(X_train)&#xa;clf = svm.SVC(C=1).fit(X_train_transformed, y_train)&#xa;X_test_transformed = scaler.transform(X_test)&#xa;clf.score(X_test_transformed, y_test)&#xa;&#xa;&#x4f7f;&#x7528;&#x7ba1;&#x7ebf;&#x5219;&#x53ef;&#x4ee5;&#x5f88;&#x7b80;&#x5355;&#xff0c;&#xa;&#xa;from sklearn.pipeline import make_pipeline&#xa;clf = make_pipeline(preprocessing.StandardScaler(), svm.SVC(C=1))&#xa;cross_val_score(clf, iris.data, iris.target, cv=cv)" ID="ID_1732416926" CREATED="1541601427302" MODIFIED="1541601589583"/>
</node>
</node>
<node TEXT="API References" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" ID="ID_714353965" CREATED="1541670550002" MODIFIED="1541670655868" LINK="https://scikit-learn.org/stable/modules/classes.html"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#20851;&#20110;sklearn&#25509;&#21475;&#30340;&#40479;&#30640;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="datasets" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1595207637" CREATED="1541514030268" MODIFIED="1541670347293"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#29992;&#20110;&#21019;&#24314;&#21508;&#31181;&#23454;&#39564;&#29992;&#25968;&#25454;&#65292;&#27604;&#36739;&#26041;&#20415;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="make_circles" FOLDED="true" ID="ID_1056019170" CREATED="1541514060339" MODIFIED="1541514063333">
<node TEXT="" ID="ID_1982429393" CREATED="1541514881285" MODIFIED="1541517139843">
<hook URI="images/make_circles.jpg" SIZE="0.7853403" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="make_classification" ID="ID_513454913" CREATED="1541515095057" MODIFIED="1541515099412"/>
<node TEXT="load_iris()" ID="ID_823810302" CREATED="1541598182266" MODIFIED="1541598289902"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21152;&#36733;&#32463;&#20856;&#25945;&#23398;&#29992;&#23454;&#39564;&#25968;&#25454;
    </p>
  </body>
</html>
</richcontent>
<node TEXT="iris = datasets.load_iris()&#xa;iris.data.shape, iris.target.shape" ID="ID_1129981055" CREATED="1541598424324" MODIFIED="1541598435477"/>
</node>
</node>
<node TEXT="ensemble" LOCALIZED_STYLE_REF="styles.topic" ID="ID_816293214" CREATED="1541515949957" MODIFIED="1541670334368">
<node TEXT="RandomForestRegressor" ID="ID_635211500" CREATED="1541515961476" MODIFIED="1541515976133"/>
<node TEXT="RandomForestClassifier" ID="ID_536984975" CREATED="1541515981395" MODIFIED="1541515987836"/>
<node TEXT="" ID="ID_149640141" CREATED="1541925987166" MODIFIED="1541925987171">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="ExtraTreesClassifier" ID="ID_837638398" CREATED="1541925886160" MODIFIED="1541925932231"/>
<node TEXT="ExtraTreesRegressor" ID="ID_1733078237" CREATED="1541925911869" MODIFIED="1541925923479"/>
<node TEXT="" ID="ID_783236130" CREATED="1541925987159" MODIFIED="1541925987166">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Extremely Randomized Trees employ a greater degree of randomization at the cut-point choice when splitting a tree node. As in random forests, a random subset of features is used. But, instead of the search for the optimal thresholds, their values are selected at random for each possible feature, and the best one among these randomly generated thresholds is used as the best rule to split the node. This usually trades off a slight reduction in the model variance with a small increase of the bias." ID="ID_571516182" CREATED="1541925987172" MODIFIED="1541925992266"/>
</node>
<node TEXT="RandomTreesEmbedding" ID="ID_1238498304" CREATED="1541926896235" MODIFIED="1541926940719"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#24212;&#29992;&#22312;&#26080;&#30417;&#30563;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="model_selection" LOCALIZED_STYLE_REF="styles.topic" ID="ID_434220777" CREATED="1541515056930" MODIFIED="1541670338167">
<node TEXT="GridSearchCV" ID="ID_237624513" CREATED="1541515120969" MODIFIED="1541515127715"/>
<node TEXT="StratifiedKFold" ID="ID_1498538162" CREATED="1541515166712" MODIFIED="1541515188674"/>
</node>
<node TEXT="metrics" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1966259970" CREATED="1541515203848" MODIFIED="1541670340126">
<node TEXT="accuracy_score" ID="ID_700045363" CREATED="1541515212558" MODIFIED="1541515216385"/>
<node TEXT="recall_score" ID="ID_23263972" CREATED="1541670154552" MODIFIED="1541670157436"/>
<node TEXT="scorer" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_847771420" CREATED="1541670288753" MODIFIED="1541670342301">
<node TEXT="make_scorer" ID="ID_494371832" CREATED="1541670368532" MODIFIED="1541670371977"/>
</node>
</node>
<node TEXT="feature_extraction" LOCALIZED_STYLE_REF="styles.topic" ID="ID_786593060" CREATED="1541945117179" MODIFIED="1541945125632">
<node TEXT="text" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_1876363145" CREATED="1541945132122" MODIFIED="1541945135779">
<node TEXT="CountVectorize" ID="ID_438266772" CREATED="1541945141474" MODIFIED="1541945229824"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23558;&#36755;&#20837;&#25991;&#26412;&#30697;&#38453;&#21270;(csr&#31232;&#30095;&#30697;&#38453;)&#65292;&#21363;&#34892;&#26159;&#27599;&#31687;&#25991;&#23383;&#65292;&#21015;&#26159;&#21333;&#35789;&#65292;&#20803;&#32032;&#26159;&#20986;&#29616;&#27425;&#25968;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="feature_selection" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1569549052" CREATED="1542186167359" MODIFIED="1542186179364" LINK="https://scikit-learn.org/stable/modules/feature_selection.html"/>
<node TEXT="preprocessing" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1277112055" CREATED="1542102953913" MODIFIED="1542102986374">
<node TEXT="StandardScalar" ID="ID_1716092574" CREATED="1542102961417" MODIFIED="1542102966667"/>
<node TEXT="MinMaxScalar" ID="ID_865042197" CREATED="1542102975520" MODIFIED="1542102979121"/>
</node>
</node>
</node>
<node TEXT="scipy" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_288773306" CREATED="1541515240207" MODIFIED="1541516136896">
<node TEXT="Sparse Matrix" ID="ID_1903932241" CREATED="1541515249311" MODIFIED="1541515274617"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#21442;&#35265;&#25105;&#30340;&#27963;&#39029;&#31508;&#35760;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="numpy" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1456592590" CREATED="1541514400671" MODIFIED="1541516142592">
<node TEXT="np.assarray" ID="ID_1998269193" CREATED="1541515636991" MODIFIED="1541515673921"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &#23558;array_like&#36716;&#25442;&#20026;np.array&#65292;&#20351;&#29992;&#26041;&#20415;&#12290;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
</map>
